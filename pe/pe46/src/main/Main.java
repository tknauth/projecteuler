package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
	private static List<Integer> primes = new ArrayList<Integer>();
	public static void main(String[] args) {
		final int LIMIT = 1000000;
		try {
			BufferedReader br = new BufferedReader(new FileReader("/home/thomas/playground/python/primes.db"));
			do {
				String line = br.readLine();
				if (null == line) break;
				int p = Integer.parseInt(line);
				if (p > LIMIT) break;
				primes.add(p);
			} while (true);
		} catch (IOException e) {
		}

		int start = 0;
		int len_max = 0;
		int p_max = 0;
		do {
			for (int i=start, sum=0, len=0; sum<LIMIT && i<primes.size(); sum+=primes.get(i), len++, i++) {
				if (Collections.binarySearch(primes, sum) > 0) {
					if (len > len_max) {
						len_max = len;
						p_max = sum;
					}
				}
			}
			start++;
		} while(start<LIMIT);
		System.out.println(""+p_max+" len= "+len_max);
	}
}
