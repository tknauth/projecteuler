package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;

// http://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Continued_fraction_expansion
// http://en.wikipedia.org/wiki/Periodic_continued_fraction
// http://en.wikipedia.org/wiki/Continued_fraction

public class Pe64 {

    /**
     * @result The sequence of numbers representing the continued fraction.
     * @param n The square root of which to compute the continued fraction for.
     */
    public static List<Integer> continuedFraction(int n) {
        List<Integer> as = new LinkedList<Integer>();
        List<Integer> ds = new LinkedList<Integer>();
        List<Integer> ms = new LinkedList<Integer>();

        ms.add(0);
        ds.add(1);
        as.add((int)Math.floor(Math.sqrt(n)));

        // If square root of n is an integer, stop right here.
        if (as.get(0)*as.get(0) == n) return as;

        do {
            int i = ms.size()-1;
            //         System.err.println("m= "+last(ms)+", d= "+last(ds)+", a= "+last(as));
            ms.add(ds.get(i)*as.get(i)-ms.get(i));
            ds.add((int)(n-ms.get(i+1)*ms.get(i+1))/ds.get(i));
            as.add((int)Math.floor((as.get(0)+ms.get(i+1))/ds.get(i+1)));

            if (last(as) == 2*as.get(0)) break;
        } while(true);
        return as;
    }

    public static void main(String[] args) {
        test();
        int c = 0;
        for (int n=2; n<=10000; n++) {
            //       System.err.println("n= "+n);
            List<Integer> r = continuedFraction(n);
            //       System.err.println("sqrt("+n+")="+r.toString());
            //       if ((n % 100) == 0) System.err.println(n);
            if ((r.size() % 2) == 0) c++;
        }
        System.out.println(""+c);
    }
    private static void test() {
    }

    public static <T> T last(List<T> cs) {
        if (cs.size()<1) return null;
        return cs.get(cs.size()-1);
    }

    public static <T> T secondLast(List<T> cs) {
        if (cs.size()<2) return null;
        return cs.get(cs.size()-2);
    }
}
