package main;

public class Main {
	public static void main(String[] args) {
		for (int nom=11; nom<100; nom++) {
			for (int denom=11; denom<100; denom++) {
				if (nom == denom) continue;
				if (nom % 10 == 0 && denom % 10 == 0) continue;
				String nom_str = Integer.toString(nom);
				String denom_str = Integer.toString(denom);
				double d = (double)nom / denom;
				for (Character c : nom_str.toCharArray()) {
					if (denom_str.indexOf(c) >= 0) {
						// some digits match
						String nom_del = new StringBuffer(nom_str).deleteCharAt(nom_str.indexOf(c)).toString();
						String denom_del = new StringBuffer(denom_str).deleteCharAt(denom_str.indexOf(c)).toString();
						double d_del = Double.parseDouble(nom_del) / Double.parseDouble(denom_del);
						if (d == d_del) {
							System.out.println(nom+"/"+denom);
						}
					}
				}
			}
		}
	}
}
