#!/usr/bin/env python2.7

import pdb

known_solutions = [[(0,1),(1,0)],
                   [(1,1),(1,0)],
                   [(0,2),(1,0)],
                   [(1,2),(1,0)],
                   [(0,1),(2,0)],
                   [(1,1),(2,0)],
                   [(2,1),(2,0)],
                   [(0,2),(2,0)],
                   [(2,2),(2,0)],
                   [(0,1),(1,1)],
                   [(0,1),(2,1)],
                   [(0,2),(1,1)],
                   [(0,2),(1,2)],
                   [(0,2),(2,2)]]

count = 0
for (xp,yp) in [(x,y) for x in range(0,51) for y in range(0,51) if not (x == 0 and y == 0)] :
    for (xq, yq) in [(x,y) for x in range(xp, 51) for y in range(0, yp+1) if (x!=xp or y!=yp) and not (x==0 and y==0)] :
        # print (xp,yp), (xq,yq)
        # if (xp,yp) == (1,1) and (xq,yq) == (1,0) :
        #     pdb.set_trace()
        c_squared = xq*xq + yq*yq
        a_squared = xp*xp + yp*yp
        b_squared = (xp-xq)*(xp-xq)+(yp-yq)*(yp-yq)

        if a_squared + b_squared == c_squared or a_squared + c_squared == b_squared or b_squared + c_squared == a_squared :
            # print (xp,yp), (xq,yq)
            # known_solutions.remove([(xp,yp),(xq,yq)])
            # print 'square triangle', (xp,yp), (xq,yq)
            count += 1

#print ''
#print known_solutions
print 'count', count
