package main;



public class Main {
	public static void main(String[] args) {
		int n_max = 0;
		int p_max = 0;
		for (int p=120; p<=1000; p++) {
			int n = 0;
			System.out.print("p= "+p);
			for (int a=1; a<p/2; a++) {
				for (int b=1; b<(p-a)/2; b++) {
					int c = p - a - b;
					if (a*a + b*b - c*c != 0) continue;
					n++;
					System.out.print(" "+a+", "+b+", "+c+";");
				}
			}
			System.out.println("");
			if (n > n_max) {
				n_max = n;
				p_max = p;
			}
		}
		System.out.println("p_max= "+p_max+", n_max= "+n_max);
	}
}
