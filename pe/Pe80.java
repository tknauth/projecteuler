package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;
import org.apache.commons.lang.ArrayUtils;
import pe.Pe64;
import pe.Pe65;

public class Pe80 {

    public static void main(String[] args) {
        test();
        int sum = 0;
        for (int i=2; i<=100; i++) {
            List<Integer> cf = Pe64.continuedFraction(i);
            System.out.println(""+cf.toString());
            if (cf.size() == 1) continue;
            BigInteger[] nom_and_denom = Pe65.nthConvergentCntdFraction(200, ArrayUtils.toPrimitive(cf.toArray(new Integer[cf.size()])));
            System.out.println(""+nom_and_denom[0].toString().length());
            System.out.println(""+nom_and_denom[1].toString().length());
            String s = division(nom_and_denom[0],
                                nom_and_denom[1],
                                100);
            System.out.println(""+nom_and_denom[0]+"/"+nom_and_denom[1]);
            for (char c : s.toCharArray()) {
                sum += c-'0';
            }
        }
        System.out.println(""+sum);
    }

    private static String division(BigInteger nom, BigInteger denom, int digits) {
        StringBuffer sb = new StringBuffer();
        for (int i=0; i<digits; ++i) {
            BigInteger digit = nom.divide(denom);
            sb.append(digit.toString());
            BigInteger rem = nom.subtract(denom.multiply(digit));
            nom = rem.multiply(BigInteger.valueOf(10));
        }
        return sb.toString();
    }

    private static void test() {
        System.out.println(division(BigInteger.valueOf(577), BigInteger.valueOf(408), 5));
    }
}
