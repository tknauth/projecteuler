package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;
import pe.Pe64;
import pe.Pe65;

public class Pe66 {

    public static int[] toArray(Collection<Integer> xs) {
        int[] r = new int[xs.size()];
        int i = 0;
        for (Integer x : xs) {
            r[i++] = x;
        }
        return r;
    }

    public static void main(String[] args) { test();
            int result_d = 13;
            BigInteger result_x = BigInteger.valueOf(9);
        for (int d=result_d+1; d<=1000; ++d) {

            // If square root of d is natural number, skip this d
            if ((long)Math.floor(Math.sqrt(d))*(long)Math.floor(Math.sqrt(d)) == d) continue;

            List<Integer> cntd_fraction = Pe64.continuedFraction(d);

            // Solutions to Pell's equation must be convergents of the
            // continued fraction for sqrt(D). Enumerate convergents
            // until one is found which solves the equation.
            for (int i=0; ; ++i) {
                BigInteger[] numDenom = Pe65.nthConvergentCntdFraction(i, toArray(cntd_fraction));
                BigInteger numerator = numDenom[0];
                BigInteger denominator = numDenom[1];

                BigInteger tmp = numerator.multiply(numerator).subtract(
                                                                        BigInteger.valueOf(d).multiply(denominator).multiply(denominator));
                // Is solution?
                if (tmp.compareTo(BigInteger.ONE) == 0) {
                    // Is solution's numerator greater than current numerator
                    if (numerator.compareTo(result_x) == 1) {
                        result_x = numerator;
                        result_d = d;
                    }
                    break;
                }
            }

        }
        System.out.println(result_d);
    }
    private static void test() {
    }
}
