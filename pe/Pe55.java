import java.math.BigInteger;

public class Pe55 {
  private static final int MAX = 100;
  public static void main(String[] args) {
//     test();
    int c = 0;
    for (int n=1; n<10000; n++) {
      if (false == isLycherel(n)) {
        c++;
      }
    }
    System.out.println(""+c);
  }
  public static boolean isLycherel(int n) {
    return isLycherel(BigInteger.valueOf(n));
  }
  public static boolean isLycherel(BigInteger n) {
    for (int i=0; i<50; i++) {
//       System.out.print(""+n.toString()+" -> ");
      n = reverseAdd(n);
      if (isPalindrom(n.toString())) {
//         System.out.println(""+n.toString());
        return true;
      }
    }
//     System.out.println("");
    return false;
  }
  public static BigInteger reverseAdd(BigInteger n) {
    BigInteger n_new = n.add(new BigInteger(reverse(n.toString())));
    //if (n_new < n) throw new RuntimeException("Overflow");
    return n_new;
  }
  private static void test() {
    System.out.println(""+isLycherel(47));
    System.out.println(""+isLycherel(349));
  }
  public static String reverse(String s) {
    StringBuffer sb = new StringBuffer();
    for (int i=s.length()-1; i>=0; i--) {
      sb.append(s.charAt(i));
    }
    return sb.toString();
  }
  public static boolean isPalindrom(String s) {
    for (int i=0; i<s.length()/2; i++) {
      if (s.charAt(i) != s.charAt(s.length()-1-i)) return false;
    }
    return true;
  }
}
