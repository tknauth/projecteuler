#!/usr/bin/env python

from math import *

max_k = 12001
max_N = 2*max_k
factors = [None]*max_N
Ns = set()

for N in range(2,max_N) :
    res = set([(N,)])
    work = [(N,)]

    while len(work) > 0 :
        next_work = []
        for t in work :
            for i in range(int(floor(sqrt(t[0]))), 1, -1) :
                if ((t[0]/i)*i == t[0]) :
                    tmp = tuple(sorted((t[0]/i, i)+t[1:], reverse=True))
                    if not tmp in res :
                        next_work.append(tmp)
                        res.add(tmp)
        work = next_work
    factors[N] = res

for k in range(2,max_k) :
    solved_this_N = False
    for N in range(k+1, 2*k+1) :
        for factor in factors[N] :
            s = sum(factor) + k - len(factor)
            if s == N :
                Ns.add(s)
                solved_this_N = True
        if solved_this_N : break

print sum(Ns)

import resource
print resource.getrusage(resource.RUSAGE_SELF).ru_maxrss, 'kb'
