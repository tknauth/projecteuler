#!/usr/bin/env python

from numpy import *
import sys
import random
import multiprocessing as mp
import pdb
from collections import defaultdict
import itertools
import copy

import signal
def  int_handler(signal, frame):
        import pdb
        pdb.set_trace()
signal.signal(signal.SIGINT, int_handler)

def copyto(dst, src) :
    assert dst.shape == src.shape
    (n, m) = dst.shape
    for row in range(n) :
        for col in range(m) :
            dst[row][col] = src[row][col]

def candidates(a, x, y) :
    cands = set(range(1,10))
    for i in range(0, 9) :
        cands.discard(a[y][i])
    for j in range(0, 9) :
        cands.discard(a[j][x])

    for i in range(0, 3) :
        for j in range(0, 3) :
            # print 'x=', x+i-(x%3), 'y=', y+j-(y%3)
            cands.discard(a[y+j-(y%3)][x+i-(x%3)])
    return cands

def read_one_sudoku(f) :
    a = zeros((9,9), dtype=int32)
    f.readline()
    for i in range(9) :
        line = f.readline()
        line = line.strip()
        for j in range(9) :
            a[i][j] = int(line[j])
    return a

def check_validity(a, cons) :
    for x in range(0,9) :
        vertical = set([])
        for y in range(0,9) :
            if a[y][x] != 0 and a[y][x] in vertical :
                return False
            else :
                vertical.add(a[y][x])

    for y in range(0,9) :
        horizontal = set([])
        for x in range(0,9) :
            if a[y][x] != 0 and a[y][x] in horizontal :
                return False
            else :
                horizontal.add(a[y][x])

    for xcell in range(0,3) :
        for ycell in range(0,3) :
            v = set()
            for x in range(xcell*3, 3) :
                for y in range(ycell*3, 3) :
                    if a[y][x] != 0 and a[y][x] in v :
                        return False
                    else :
                        v.add(a[y][x])

    for (x,y) in itertools.product(range(9), range(9)) :
        if (a[y][x] == 0 and len(cons[(x,y)])  > 0) or \
           (a[y][x] != 0 and len(cons[(x,y)]) == 0) :
            pass
        else :
            return False

    return True

def count_possible_next_moves(a, x, y, c) :
    a = a.copy()
    a[y][x] = c

    res = 1

    for x in range(0,9) :
        for y in range(0,9) :
            if a[y][x] != 0 : continue
            res *= len(candidates(a, x, y))
    return res

def numbers_in_block(a, block_x, block_y) :
    r = set()
    for x in range(block_x*3, block_x*3+3) :
        for y in range(block_y*3, block_y*3+3) :
            if a[y][x] != 0 :
                r.add(a[y][x])
    return r

def n_in_column(a, x, n) :
    assert n >= 0 and n <= 9
    return n in set([a[y][x] for y in range(0,9)])

def n_in_row(a, y, n) :
    assert n >= 0 and n <= 9
    return n in set([a[y][x] for x in range(0,9)])

def slice_and_dice(a) :
    cand = defaultdict(set)
    for block_x in range(0,3) :
        for block_y in range(0,3) :
            for n in set(range(1,9)) - numbers_in_block(a, block_x, block_y) :
                rows = set(range(block_y*3, block_y*3+3))
                cols = set(range(block_x*3, block_x*3+3))
                for x in range(block_x*3, block_x*3+3) :
                    if n_in_column(a, x, n) :
                        cols.remove(x)
                for y in range(block_y*3, block_y*3+3) :
                    if n_in_row(a, y, n) :
                        rows.remove(y)
                if len(rows) == 1 and len(cols) == 1 and a[list(rows)[0]][list(cols)[0]] == 0 :
                    cand[(cols.pop(),rows.pop())].add(n)

    mod = False
    for (k,v) in cand.iteritems() :
        if len(v) == 1 :
            print 'found slice and dice!'
            (x,y) = k
            print 'x=', x, 'y=', y, v
            a[y][x] = v.pop()
            mod = True

    return mod

# def single_square_candidate(a) :
#     for box in range(0,9) :
#         for i in range(1,10) :
#             if len([(x,y) for (x,y) in box2cells(box) and i in cands(a[y][x])]) == 1 :
                

def no_more_moves(a) :
    for x in range(0,9) :
        for y in range(0, 9) :
            if a[y][x] == 0 and len(candidates(a,x,y)) == 0 :
                return True
    return False

def possible_moves(a) :
    r = set()
    for x in range(0,9) :
        for y in range(0, 9) :
            if a[y][x] == 0 :
                cands = candidates(a,x,y)
                if len(cands) > 1 :
                    for c in cands :
                        r.add((x,y,c))
    return r

def solve_singleton(a) :
    for x in range(0,9) :
        for y in range(0, 9) :
            if a[y][x] != 0 : continue
            cands = candidates(a, x, y)
            if 1 == len(cands) :
                print 'found singleton: x=', x, 'y=', y, cands
                a[y][x] = cands.pop()
                return True
    return False

def solve_sudoku(a, invalid = set([]), depth=0) :
    print 'depth', depth
    # print 'invalid', invalid

    if is_solved(a) :
        print a
        return True

    if no_more_moves(a) : return False

    while solve_singleton(a) :
        if not check_validity(a) :
            pdb.set_trace()

    # if depth == 24 : pdb.set_trace()

    while slice_and_dice(a) :
        if not check_validity(a) :
            pdb.set_trace()

    moves = sorted(possible_moves(a),
                   key = lambda (x,y,c) : count_possible_next_moves(a, x, y, c), reverse=False)
    # possible_moves = sorted(possible_moves, key = lambda (x,y,c) : random.randint(0,100000), reverse=True)
    # possible_moves = sorted(possible_moves, key = lambda (x,y,c) : x+y)
    # possible_moves = sorted(possible_moves,
    #                         key = lambda (x,y,c) : len(filter(lambda (xx,yy,cc) : c == cc, possible_moves)))
    print 'alternatives', len(moves)
    # print possible_moves

    local_invalid = invalid.copy()

    for (x, y, c) in moves :
        if (x,y,c) in local_invalid : continue
        print 'trying ', x, y, c
        a_guess = array(a)
        a_guess[y][x] = c
        # print x,y,c
        # print a
        # print a_guess
        # sys.exit(1)
        if solve_sudoku(a_guess, local_invalid, depth+1) :
            copyto(a, a_guess)
            return True
        else :
            local_invalid.add((x,y,c))

    return False

def is_solved(a) :
    for x in range(9) :
        for y in range(9) :
            if 0 == a[y][x] : return False
    return True

def box(x,y) :
    return (y/3)*3 + x/3

def box2cells(n) :
    return [(x,y) for x in range((n%3)*3, (n%3)*3+3) for y in range((n/3)*3, (n/3)*3+3)]

units = [ [(x, y) for x in range(9)] for y in range(9) ] + \
        [ [(x, y) for y in range(9)] for x in range(9) ] + \
        [ box2cells(n) for n in range(9) ]

xy_all = [(x, y) for x in range(9) for y in range(9)]

def assign_to_square(grid, cons, x, y, v) :
    grid[y][x] = v
    cons[(x,y)].clear()
    
    for i in range(9) : cons[(x,i)].discard(v)
    for i in range(9) : cons[(i,y)].discard(v)
    for (bx,by) in box2cells(box(x,y)) : cons[(bx,by)].discard(v)

# If exactly two squares in a unit share a common candidate, this candidate can be removed from all other squares 
def squares_share_two_candidates(grid, cons) :
    # pdb.set_trace()
    for u in units :
        for squares in itertools.combinations(u, 2) :
            assert len(squares) == 2
            shared_cands = cons[squares[0]].intersection(cons[squares[1]])
            if not len(shared_cands) == 2 : continue
            # shared candidates not present in any other square of this unit
            shared = False
            for (x,y) in u :
                if (x,y) == squares[0] or (x,y) == squares[1] : continue
                if len(shared_cands & cons[(x,y)]) > 0 :
                    shared = True
                    break
            if shared : continue

            # Remove other cands from sharing squares
            print 'squares_share_two', squares, cons[squares[0]], cons[squares[1]]
            cons[squares[0]] = shared_cands.copy()
            cons[squares[1]] = shared_cands.copy()

# http://theory.tifr.res.in/~sgupta/sudoku/algo.html
# TODO extend to locked canidates for two rows/units
def locked_candidates(grid, cons) :
    mod = False
    for hg in range(3) :
        for vg in range(3) :
            # range(6) for locked_candidates_ext two primary cols/rows
            # (instead of one); only a single secondary col/row
            cols = [hg*3+i for i in range(3)]
            rows = [vg*3+i for i in range(3)]

            for primary_col in cols :
                secondary_cols = set(cols) - set([primary_col])
                primary_candidates = set()
                for (x,y) in itertools.product([primary_col], rows) :
                    # print 'primary', x, y
                    primary_candidates = primary_candidates | cons[(x,y)]
                unique_candidates = set(primary_candidates)
                for (x,y) in itertools.product(secondary_cols, rows) :
                    # print 'secondary', x,y
                    unique_candidates = unique_candidates - cons[(x,y)]

                if len(unique_candidates) > 0 :
                    non_group_rows = set(range(9)) - set(rows)
                    for (x,y) in itertools.product([primary_col], non_group_rows) :
                        print 'removing', unique_candidates, 'from', x, y
                        if len(cons[(x,y)] & unique_candidates) > 0 :
                            # pdb.set_trace()
                            mod = True
                        cons[(x,y)] -= unique_candidates
                        
            for primary_col in cols :
                secondary_cols = set(cols) - set([primary_col])
                primary_candidates = set()
                for (y,x) in itertools.product([primary_col], rows) :
                    # print 'primary', x, y
                    primary_candidates = primary_candidates | cons[(x,y)]
                unique_candidates = set(primary_candidates)
                for (y,x) in itertools.product(secondary_cols, rows) :
                    # print 'secondary', x,y
                    unique_candidates = unique_candidates - cons[(x,y)]

                if len(unique_candidates) > 0 :
                    non_group_rows = set(range(9)) - set(rows)
                    for (y,x) in itertools.product([primary_col], non_group_rows) :
                        print 'removing', unique_candidates, 'from', x, y
                        if len(cons[(x,y)] & unique_candidates) > 0 :
                            # pdb.set_trace()
                            mod = True
                        cons[(x,y)] -= unique_candidates

    return mod

# check if a candidate appears in only one square (per unit). This is
# different from "a square has exactly one candidate".
def single_square_candidate(grid, cons) :
    mod = False
    for u in units :
        count = defaultdict(set)
        for coord in u :
            for cand in cons[coord] :
                count[cand].add(coord)
        for k in count.keys() :
            if len(count[k]) == 1 :
                print 'single_square_candidate found!', count[k], k
                mod |= True
                # pdb.set_trace()
                (x,y) = list(count[k])[0]
                assign_to_square(grid, cons, x, y, k)
    return mod

def flatten(listOfLists):
    "Flatten one level of nesting"
    return list(itertools.chain(*listOfLists))

def pp_cons(cons) :
        for y in range(9) :
                for x in range(9) :
                        for v in range(1,10) :
                                if v in cons[(x,y)] : print v,
                                else : print ' ',
                        print '|',
                print ''

def locked_candidates_ext(grid, cons) :
    mod = False
    for horizontal_units in itertools.combinations(range(3), 2) :
        # print horizontal_units
        for vertical_unit in range(3) :
            for rows in itertools.combinations(range(vertical_unit*3, vertical_unit*3+3), 2) :
                # print 'rows', rows
                #
                # each unit must have the candidates in two rows/cols
                # but not in the third row/col
                odd_row_out = (set(range(vertical_unit*3, vertical_unit*3+3))-set(rows)).pop()
                u1_xs = [horizontal_units[0]*3, horizontal_units[0]*3+1, horizontal_units[0]*3+2]
                u2_xs = [horizontal_units[1]*3, horizontal_units[1]*3+1, horizontal_units[1]*3+2]
                # pdb.set_trace()
                u1_shared = (set(flatten([cons[(x,y)] for (x,y) in itertools.product(u1_xs, [rows[0]])])) & \
                            set(flatten([cons[(x,y)] for (x,y) in itertools.product(u1_xs, [rows[1]])]))) - \
                            set(flatten([cons[(x,y)] for (x,y) in itertools.product(u1_xs, [odd_row_out])]))
                u2_shared = set(flatten([cons[(x,y)] for (x,y) in itertools.product(u2_xs, [rows[0]])])) & \
                            set(flatten([cons[(x,y)] for (x,y) in itertools.product(u2_xs, [rows[1]])])) - \
                            set(flatten([cons[(x,y)] for (x,y) in itertools.product(u2_xs, [odd_row_out])]))

                shared_not_in_odd_row = u1_shared & u2_shared

                if len(shared_not_in_odd_row) > 0 :
                    # pdb.set_trace()
                    # don't share any candidates!
                    # remove shared candidates non-xs squares in rows
                    for (x,y) in itertools.product(set(range(9))-set(u1_xs+u2_xs), rows) :
                        if len(cons[(x,y)] & shared_not_in_odd_row) > 0 :
                            if len(cons[(x,y)] - shared_not_in_odd_row) == 0 :
                                assert False
                            mod |= True
                            pdb.set_trace()
                            cons[(x,y)] -= shared_not_in_odd_row

                u1_shared = (set(flatten([cons[(x,y)] for (y,x) in itertools.product(u1_xs, [rows[0]])])) & \
                            set(flatten([cons[(x,y)] for (y,x) in itertools.product(u1_xs, [rows[1]])]))) - \
                            set(flatten([cons[(x,y)] for (y,x) in itertools.product(u1_xs, [odd_row_out])]))
                u2_shared = set(flatten([cons[(x,y)] for (y,x) in itertools.product(u2_xs, [rows[0]])])) & \
                            set(flatten([cons[(x,y)] for (y,x) in itertools.product(u2_xs, [rows[1]])])) - \
                            set(flatten([cons[(x,y)] for (y,x) in itertools.product(u2_xs, [odd_row_out])]))

                shared_not_in_odd_row = u1_shared & u2_shared

                if len(shared_not_in_odd_row) > 0 :
                    # pdb.set_trace()
                    # don't share any candidates!
                    # remove shared candidates non-xs squares in rows
                    for (y,x) in itertools.product(set(range(9))-set(u1_xs+u2_xs), rows) :
                        if len(cons[(x,y)] & shared_not_in_odd_row) > 0 :
                            if len(cons[(x,y)] - shared_not_in_odd_row) == 0 :
                                # This should never happen. After
                                # eliminating candidates and unsolved
                                # square has no remaining
                                # candidates. Can also mean that we
                                # guessed wrong.
                                assert False
                            mod |= True
                            pdb.set_trace()
                            cons[(x,y)] -= shared_not_in_odd_row

                # coord = [(y,x) for (x,y) in itertools.product(xs, rows)]
    return mod

def solve_sudoku2(grid, cons=None, lvl=0) :

    print 'lvl', lvl

    # Compute constrains for each square based unit-uniqueness per
    # row/column/box.
    if not cons :
            cons = defaultdict(set)
            for (x,y) in [(x,y) for x in range(0,9) for y in range(0,9)] :
                if grid[y][x] != 0 :
                    cons[(x,y)] = set([])
                else :
                    cons[(x,y)] = set(range(1,10))
                    cons[(x,y)] -= set([grid[y][i] for i in range(0,9)])
                    cons[(x,y)] -= set([grid[i][x] for i in range(0,9)])
                    cons[(x,y)] -= set([grid[square_y][square_x] for (square_x,square_y) in box2cells(box(x,y))])

    # print grid
    # for (x,y) in [(x,y) for x in range(9) for y in range(9)] :
    #     print (x,y), cons[(x,y)]

    mod = True
    while mod :
        mod = False
        for (x,y) in [(x,y) for (x,y) in cons.keys() if len(cons[(x,y)]) == 1] :
            mod = True
            # print x,y, cons[(x,y)]
            assert grid[y][x] == 0
            assert len(cons[(x,y)]) == 1

            assign_to_square(grid, cons, x, y, list(cons[(x,y)])[0])
            if not check_validity(grid, cons) :
                return None
            else :
                break
        
            # print grid
            # for (x,y) in [(x,y) for x in range(9) for y in range(9)] :
            #     print (x,y), cons[(x,y)]

        # slice and dice (only for x)
        for x in range(9) :
            for n in range(1,10) :
                candidate_cells = [(x,i) for i in range(9) if n in cons[(x,i)]]
                if len(candidate_cells) == 1 :
                    # print 'single candidate for %d'%n, candidate_cells
                    (candx, candy) = candidate_cells[0]
                    assign_to_square(grid, cons, candx, candy, n)
                    mod = True


        for y in range(9) :
            for n in range(1,10) :
                candidate_cells = [(i,y) for i in range(9) if n in cons[(i,y)]]
                if len(candidate_cells) == 1 :
                    # print 'single candidate for %d'%n, candidate_cells
                    (candx, candy) = candidate_cells[0]
                    assign_to_square(grid, cons, candx, candy, n)
                    mod = True

        # two squares in a unit have same two candidates; candidate
        # appears in no other square in same unit
        if squares_share_two_candidates(grid, cons) :
            print 'squares_share_two_candidates'
            mod = True

        mod |= locked_candidates(grid, cons)
        mod |= single_square_candidate(grid, cons)
        # mod |= locked_candidates_ext(grid, cons)


    # mod == False, but puzzle may not be solved yet. Is it time for
    # backtracking yet?!
    unsolved_squares = int(sum([1 for (x,y) in xy_all if grid[y][x] == 0]))
    print 'unsolved square', unsolved_squares
    if unsolved_squares == 0 :
        # pdb.set_trace()
        pass

    # print grid
    if not check_validity(grid, cons) :
        return None
    
    
    if not is_solved(grid) :
        # pick random next move, update grid and cons accordingly
        # was "... in intertools.permutations(range(9), 2) ..."
        unsolved_squares = [(x,y) for (x,y) in xy_all if grid[y][x] == 0]
        for (randx, randy) in sorted(unsolved_squares, key=lambda x : len(cons[x])):
            for v in cons[(randx, randy)] :
                # pdb.set_trace()
                print 'backtracking', randx, randy, v, 'len(cons[x,y])', len(cons[(randx,randy)])
                # Guessing may introduce invalid "solutions" which violate
                # implementation constraints.
                new_grid = grid.copy()
                new_cons = copy.deepcopy(cons)
                assign_to_square(new_grid, new_cons, randx, randy, v)
                solution = solve_sudoku2(new_grid, new_cons, lvl+1)
                if solution != None :
                    assert is_solved(solution)
                    return solution
        return None
    else :
        return grid
                
    #     print 'open choices: ', sum( [len(cons[(x,y)]) for (x,y) in itertools.combinations(range(0,9), 2)] )

# Introduce second board to record available numbers for each square.

for n in range(9) :
    for (x,y) in box2cells(n) :
         assert box(x,y) == n
         
f = open('pe96-input.txt')
solved = 0

s = 0
for i in range(50) :
    print 'puzzle %d'%(i), '='*60
    a = read_one_sudoku(f)
    a = solve_sudoku2(a)
    if is_solved(a) : solved += 1
    else : print 'not solved %d'%(i)

    s += int(str(a[0][0])+str(a[0][1])+str(a[0][2]))

print 'solved %d/50'%(solved)
print 's', s
