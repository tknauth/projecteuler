package main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

public class Main {
	private static Set<Integer> primes = new TreeSet<Integer>();
	private static final int LIMIT = 1000000;
	public static void main(String[] args) {
		
		try {
			BufferedReader br = new BufferedReader(new FileReader("/home/thomas/playground/python/primes.db"));
			int p;
			do {
				String line = br.readLine();
				if (null == line) break;
				p = Integer.parseInt(line);
				primes.add(p);
			} while(p<LIMIT);
		} catch (FileNotFoundException e) {
			
		} catch (IOException e) {
			
		}
		// circular primes counter
		int c = 0;
		for (int p : primes) {
			boolean all_primes = true;
			for (int r : rotates(p)) {
				all_primes &= primes.contains(r);
			}
			if (all_primes) {
				//System.out.println(""+p);
				c++;
			}
		}
		System.out.println(""+c);
	}
	private static int[] rotates(int n) {
		String n_str = Integer.toString(n);
		int[] rots = new int[n_str.length()-1];
		StringBuffer sb = new StringBuffer(n_str);
		for (int i=0; i<rots.length; i++) {
			sb.insert(0, sb.charAt(sb.length()-1));
			sb.delete(sb.length()-1, sb.length());
			rots[i] = Integer.parseInt(sb.toString());
		}
		return rots;
	}
}
