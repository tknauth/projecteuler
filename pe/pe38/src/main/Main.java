package main;

import java.util.Arrays;


public class Main {
	public static void main(String[] args) {
		for (int n=1; n<10; n++) f(n);
		for (int n=25; n<34; n++) f(n);
		for (int n=100; n<334; n++) f(n);
		for (int n=5000; n<10000; n++) f(n);
	}
	private static void f(int n) {
		StringBuffer sb = new StringBuffer("");
		int i=1;
		do {
			sb.append(Integer.toString(i*n));
			i++;
		} while (sb.length()<8);
		if (sb.length()!=9) return;
		boolean[] digits = new boolean[10];
		Arrays.fill(digits, false);
		digits[0] = true;
		for (char c : sb.toString().toCharArray()) {
			if (digits[c-'0']) return;
			digits[c-'0'] = !digits[c-'0'];
		}
		// is pandigital
		System.out.println(n+" : "+sb.toString());
	}
}
