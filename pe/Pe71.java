package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;
import pe.Pe58;

public class Pe71 {

    public static void main(String[] args) {
        test();

        final int d_limit = 1000000;
        int n = 3;
        int d = 8;
        int cand_n = n;
        int cand_d = d;
        for (; d<=d_limit; ++d) {
            while (d*3 > n*7) ++n;
            --n;
            // System.out.println(n+"/"+d);
            if (cand_n * d < cand_d * n) {
                cand_n = n;
                cand_d = d;
            }
        }
        System.out.println(cand_n+"/"+cand_d+" "+gcd(cand_n, cand_d));
    }

    public static int gcd(int a, int b) {
        if (b > a) {
            int tmp = a;
            a = b;
            b = tmp;
        }
        assert a >= b;

        do {
            int r = a - ((a / b) * b);
            if (r == 0) return b;
            a = b;
            b = r;
        } while (a >= b);

        return 1;
    }

    private static void test() {
        assert gcd(8,3) == 1;
        assert gcd(8,4) == 4;
        assert gcd(4,8) == 4;
        assert gcd(4,16) == 4;
        assert gcd(1071,462) == 21;
    }
}
