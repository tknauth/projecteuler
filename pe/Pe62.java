package pe;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.LinkedList;

public class Pe62 {
  public static void main(String[] args) {
    test();
    Map<String, List<Integer>> m = new HashMap<String, List<Integer>>();
    int n = 1;
    do {
      BigInteger nCubed = BigInteger.valueOf(n).pow(3);
      char[] nCubedStr = nCubed.toString().toCharArray();
      Arrays.sort(nCubedStr);
      String sorted = new String(nCubedStr);
      List<Integer> list = m.get(sorted);
      if (null == list) {
        list = new LinkedList<Integer>();
        m.put(sorted, list);
      }
      list.add(n);
      if (list.size() == 5) {
        int tmp = list.get(0);
        System.out.println(BigInteger.valueOf(tmp).pow(3));
        break;
      }
      n++;
    } while (true);
  }
  private static void test() {
    String a = "41063625", b = "56623104", c = "66430125", d = "11111111";
    assert isPermutation(a, b);
    assert isPermutation(b, c);
    assert false == isPermutation(a, d);
  }
  public static boolean isCube(int n) {
    return false;
  }
  public static boolean isPermutation(String a, String b) {
    return isPermutation(a.toCharArray(), b.toCharArray());
  }
  public static boolean isPermutation(char[] a, char[] b) {
    Arrays.sort(a);
    Arrays.sort(b);
    for (int i=0; i<a.length; i++) {
      if (a[i] != b[i]) return false;
    }
    return true;
  }
}
