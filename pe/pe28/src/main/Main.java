package main;

public class Main {
	private static final int LIMIT=(1001-1)/2;
	public static void main(String[] args) {
		int sum = 0;
		int toAdd = 1;
		int d = 2;
		for (int i=0; i<LIMIT; i++) {
			for (int j=0; j<4; j++) {
				sum += toAdd;
				toAdd += d;
			}
			d += 2;
		}
		sum += toAdd;
		System.out.println("sum= "+sum);
	}
}
