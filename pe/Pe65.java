package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;

public class Pe65 {

    /**
     * @return [0] is numerator, [1] is denominator
     */
    public static BigInteger[] nthConvergentCntdFraction(int n, int[] s) {
        BigInteger num = BigInteger.ZERO;
        BigInteger denom = BigInteger.ONE;
        for (int i=n-2; i>=0; --i) {
            BigInteger num_i = num;
            BigInteger denom_i = denom;

            num = denom_i;
            denom = num_i.add(denom_i.multiply(BigInteger.valueOf(s[1+(i%(s.length-1))])));
        }
        return new BigInteger[] {BigInteger.valueOf(s[0]).multiply(denom).add(num), denom};
    }

    public static BigInteger[] nthConvergentCntdFraction(int[] s) {
        return nthConvergentCntdFraction(s.length, s);
    }

    public static int[] gen_e(int n) {
        int e[] = new int[n];
        e[0] = 2;
        for (int i=1; i<e.length; ++i) {
            if ((i-1) % 3 == 0) e[i] = 1;
            else if ((i-1) % 3 == 1) e[i] = ((i-2)/3+1)*2;
            else if ((i-1) % 3 == 2) e[i] = 1;
            else { throw new RuntimeException(); }
        }
        return e;
    }

  public static void main(String[] args) {
    test();
    BigInteger[] a = nthConvergentCntdFraction(gen_e(100));
    int digit_sum = 0;
    String s = a[0].toString();
    for (int i=0; i<s.length(); ++i) {
        digit_sum += s.charAt(i) - '0';
    }
    System.out.println(digit_sum);
  }
  private static void test() {
//       System.out.println(Arrays.toString(gen_e(1)));
//       System.out.println(Arrays.toString(gen_e(2)));
//       System.out.println(Arrays.toString(gen_e(3)));
//       System.out.println(Arrays.toString(gen_e(4)));
//       System.out.println(Arrays.toString(gen_e(5)));
//       System.out.println(Arrays.toString(gen_e(6)));
//       System.out.println(Arrays.toString(gen_e(7)));
//       System.out.println(Arrays.toString(gen_e(8)));
//       System.out.println(Arrays.toString(gen_e(9)));
  }
}
