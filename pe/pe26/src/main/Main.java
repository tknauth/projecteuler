package main;

import java.util.Arrays;

public class Main {
	private static final int LIMIT = 1000;
	public static void main(String[] args) {
		int longestCycle = 0;
		int longestCycleD = 0;
		for (int d=2; d<LIMIT; d++) {
			int[] seen = new int[d];
			Arrays.fill(seen, -1);
			int nom = 1;
			for (int i=0; i<LIMIT; i++) {
				if (seen[nom] >= 0) {
					int thisCycle = i-seen[nom]+1;
					if (thisCycle > longestCycle) {
						longestCycle = thisCycle;
						longestCycleD = d;
					}
					break;
				}
				seen[nom] = i;
				if (nom < d) nom *= 10;
				int dividend = nom / d;
				nom -= dividend * d;
				if (0 == nom) break;
			}
		}
		System.out.println("longestCycle= "+longestCycle+" for d= "+longestCycleD);
	}
}
