
def rects(xdim, ydim) :
    ret = []
    for x in range(1,xdim+1) :
        for y in range(1, ydim+1) :
            ret += [(x,y)]
    return ret

dif = 2000000
sol = (0,0)
maxdim = 100
for x in range(1,maxdim) :
    for y in range(x,maxdim) :
        s = 0
        #print rects(x,y)
        for (rx,ry) in rects(x,y) :
            s += (x-rx+1)*(y-ry+1)
            #print (rx,ry), (x-rx+1)*(y-ry+1)
        #print (x,y), s
        if (abs(s-2000000)) < dif :
            dif = abs(s-2000000)
            sol = (x,y)

print sol, dif
