import random
from operator import itemgetter, attrgetter
limit = 1000000

d=[0,0]

pos=0
fin=[0 for x in range(0,40)]

cc=[2,17,33] # cc ... comunity chest
ch=[7,22,36] # ch ... chance

cccards = [lambda x : x for y in range(0,14)]
cccards += [lambda x : 0, lambda x : 10]
random.shuffle(cccards)

def next_r(x) :
    if x < 5 : return 5
    if x < 15 : return 15
    if x < 25 : return 25
    if x < 35 : return 35
    return 5

def next_u(x) :
    if x < 12 : return 12
    if x < 28 : return 28
    return 12
    
chcards = [lambda x : x for y in range(0,6)]
chcards += [lambda x : 0] # advance to go
chcards += [lambda x : 10] # go to jail
chcards += [lambda x : 11] # go to c1
chcards += [lambda x : 24] # 
chcards += [lambda x : 39] # 
chcards += [lambda x : 5] # 
chcards += [next_r] # next R railway company
chcards += [next_r] # next R railway company
chcards += [next_u] # next U utility company
chcards += [lambda x : (x-3)%40 ] # back 3 squares
random.shuffle(chcards)

doubles=0

for i in range(0, limit) :
    d[0]=random.randint(1,4)
    d[1]=random.randint(1,4)

    if d[0] == d[1] :
        doubles +=1
    else :
        doubles = 0

    if doubles == 3 :
        pos = 10
    else :
        pos = (pos + d[0] + d[1])%40
        if pos in cc :
            card = cccards.pop(0)
            pos = card(pos)
            cccards.append(card)
        elif pos in ch :
            card = chcards.pop(0)
            pos = card(pos)
            chcards.append(card)
        elif pos == 30 :
            pos = 10
        
    fin[pos] += 1

print sorted([(i, fin[i]/float(limit)) for i in range(0, len(fin))], key=itemgetter(1))
