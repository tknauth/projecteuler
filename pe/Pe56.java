import java.math.BigInteger;

public class Pe56 {
  private static final int MAX = 100;
  public static void main(String[] args) {
    int maxsum = 0;
    for (int a=1; a<MAX; a++) {
      for (int b=1; b<MAX; b++) {
        BigInteger pow = BigInteger.valueOf(a).pow(b);
        int sum = 0;
        for (Character c : pow.toString().toCharArray()) {
          sum += c - '0';
        }
        maxsum = Math.max(sum, maxsum);
      }
    }
    System.out.println(""+maxsum);
  }
  private static void test() {
    System.out.println("");
    System.out.println("");
  }
}
