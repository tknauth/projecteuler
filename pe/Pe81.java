package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;
// import org.apache.commons.lang.ArrayUtils;

public class Pe81 {
    private static final int default_size = 80;
    private static int size;

    // My first approach does not scale with increasing problem size.
    // Must use dynamic programming, remembering the minimal cost with
    // which a square can be reached.

    private static class Solution implements Comparable<Solution> {
        int cost;
        int x;
        int y;

        Solution() {
            cost = x = y = 0;
        }
        Solution(int cost) {
            x = y = 0;
            this.cost = cost;
        }
        public int compareTo(Solution o) {
            return this.cost - o.cost;
        }
    }

    private static int[][] readInput() {
        int[][] a = new int[size][size];
        try {
            BufferedReader br = new BufferedReader(new FileReader("pe81-input.txt"));
            int row = 0;
            do {
                int col = 0;
                String s = br.readLine();
                if (null == s) return a;
                Arrays.fill(a[row], 0);
                String[] split = s.split(",");
                // System.err.println(s);
                for (String nr : split) {
                    a[row][col] = Integer.parseInt(nr);
                    col++;
                }
                row++;
            } while (true);
        } catch (FileNotFoundException e) {
            System.exit(1);
        } catch (IOException e) {
            System.exit(1);
        }
        throw new RuntimeException();
    }

    public static void main(String[] args) {
        test();
        // int[][] a = new int[size][size];
        if (args.length > 0) {
            size = Integer.parseInt(args[0]);
        }
        System.out.println(""+size);

        int[][] a = readInput();
        
        for (int i=1; i<size; ++i) {
            for (int x=0; x<=i; ++x) {
                final int y = i-x;
                a[x][y] += Math.min(y > 0 ? a[x][y-1] : Integer.MAX_VALUE, x > 0 ? a[x-1][y] : Integer.MAX_VALUE);
                System.out.println(""+a[x][y]+" x="+x+" y="+y);
            }
        }

        System.out.println("------");
        for (int i=1; i<size; ++i) {
            for (int y=i, j=1; y<size; ++y, ++j) {
                final int x = size-j;
                a[x][y] += Math.min(y > 0 ? a[x][y-1] : Integer.MAX_VALUE, x > 0 ? a[x-1][y] : Integer.MAX_VALUE);
                System.out.println(""+a[x][y]+" x="+x+" y="+y);
            }
        }

        System.out.println(""+a[size-1][size-1]);
    }

    private static void test() {
    }
}
