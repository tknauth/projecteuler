package main;

import java.util.Calendar;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int d = 1, m = 1, y = 1901, dow=2;
		int result = 0, result2 = 0;
		Calendar c=Calendar.getInstance();
		c.set(1901, 1, 1);
		
		do {
			// count Sundays on first of month
			if (d==1 && dow==7) result++;
			if (c.get(Calendar.DAY_OF_MONTH)==1 &&c.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY) result2++;
			if (result != result2) {
				System.out.println("OUCH!!!");
			}
			d++;
			c.add(Calendar.DAY_OF_MONTH, 1);
			if (m==12 && d==32) { y++; m=1; d=1; }
			if ((m==1 || m==3 || m==5 || m==7 || m==8 || m==10) && d==32) {d=1; m++;}
			if ((m==4 || m==6 || m==9 || m==11) && d==31) {d=1; m++;}
			if (m==2 && isLeapYear(y) && d==30) {m++; d=1;}
			if (m==2 && !isLeapYear(y) && d==29) {m++; d=1;}
			dow = dow == 7 ? 1 : dow+1;
			
			if (y==2001 && m==1 && d==1) break;
		} while (true);
		
		System.out.println(""+result+" "+result2);
	}
	
	private static boolean isLeapYear(int i) {
		// A leap year occurs on any year evenly divisible by 4,
		// but not on a century unless it is divisible by 400
		if (i%4 == 0 && (i%100 != 0 || i%400==0)) return true;
		return false;
	}

}
