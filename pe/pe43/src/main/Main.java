package main;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		int[] divs = {17, 13, 11, 7, 5, 3, 2};
		List<List<String>> ns = new ArrayList<List<String>>();
		ns.add(0, f(divs[0]));
		for (int i=1; i<divs.length; i++) {
			ns.add(i, g(ns.get(i-1), divs[i]));
			System.out.println(ns.get(i).toString());
		}
	}
	private static List<String> f(int d) {
		List<String> ns = new LinkedList<String>();
		int n=0;
		do {
			n+=d;
			String s = Integer.toString(n);
			if (s.length()<2) continue;
			if (s.length()<3) s = '0'+s;
			if (s.charAt(0)==s.charAt(1) ||
				s.charAt(0)==s.charAt(2) ||
				s.charAt(1)==s.charAt(2)) continue;
			ns.add(s);
		} while(n<1000-d);
		return ns;
	}
	private static List<String> g(List<String> ns, int div) {
		List<String> res = new LinkedList<String>();
		for (String n : ns) {
			next_digit: for (int d=0; d<10; d++) {
				for (char c : n.toCharArray()) if (c-'0'==d) continue next_digit;
				int m = d*100+(n.charAt(0)-'0')*10+n.charAt(1)-'0';
				if (m%div==0) res.add(Integer.toString(d)+n);
			}
		}
		return res;
	}
}
