package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;
import pe.Pe58;
import pe.Pe70;

public class Pe73 {

    public static void main(String[] args) {
        test();

        final int d_limit = args.length > 0 ? Integer.parseInt(args[0]) : 12000;
        
        final int n_lower = 1;
        final int d_lower = 3;
        final int n_upper = 1;
        final int d_upper = 2;

        long s = 0;
        int n_init = n_lower;
        // This basically as quadratic complexity in d_limit, which is
        // why it does not scale at all with increasing d_limit
        for (int d=d_lower+1; d<=d_limit; ++d) {
            if (n_init*d_lower < d*n_lower) ++n_init;
            for (int n=n_init; (n*d_upper < d*n_upper); ++n) {
                // n/d > n_lower/d_lower <-> n*d_lower > d*n_lower
                if ((n*d_lower > d*n_lower)) {
                    if (Pe72.gcd(n, d) == 1) ++s;
                }
            }
        }
        System.out.println(""+s);
    }

    private static void test() {
    }
}
