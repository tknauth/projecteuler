#!/usr/bin/env python

import itertools

digits = [str(x) for x in range(0,10)]

squares = ['01', '04', '09', '16', '25', '36', '49', '64', '81']

# extend the set with a six or nine if it already contains a 9 or six,
# respecitvely.
def six_and_nine_equivalence(s) :
    if '6' in s and not '9' in s :
        return s+('9',)
    elif '9' in s and not '6' in s :
        return s+('6',)
    else :
        return s

# Exhaustively generate all cubes and check if all squares can be
# generated. Brute force works because there are only 10!/(6!*4!) =
# 210 possible cubes. With two cubes there are only 210^2 ~ 40k
# permutations to test.
c = 0
for e in itertools.combinations(digits, 6) :
    e = six_and_nine_equivalence(e)
    for f in itertools.combinations(digits, 6) :
        f = six_and_nine_equivalence(f)
        all_possible = 1
        for s in squares :
            if not ((s[0] in e and s[1] in f) or (s[0] in f and s[1] in e)) :
                all_possible = 0
                break
        c += all_possible

# divide by 2 because we don't care about the cube's order
print c/2
