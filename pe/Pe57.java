package pe;

import java.math.BigInteger;

public class Pe57 {
  public static void main(String[] args) {
    test();
    int c = 0;
    for (int i=0; i<1000; i++) {
      BigInteger nom = BigInteger.ONE, denom = BigInteger.valueOf(2);
      // 1 + 1/2
      // 1 + 1/(2 + 1/2)
      // 1 + 1/(2 + 1/(2 + 1/2))
      for (int j=0; j<i; j++) {
        BigInteger tmp = denom.multiply(BigInteger.valueOf(2)).add(nom);
        nom = denom;
        denom = tmp;
      }
      nom = nom.add(denom);
      //System.out.println(nom+"/"+denom);
      if (nom.toString().length() > denom.toString().length()) {
        c++;
      }
    }
    System.out.println(""+c);
  }
  private static void test() {
    System.out.println("");
    System.out.println("");
  }
}
