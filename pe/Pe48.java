import java.math.BigInteger;

public class Pe48 {
    public static void main(String[] args) {
        BigInteger sum = BigInteger.ZERO;
        for (int i=1; i<=1000; i++) {
            sum = sum.add(BigInteger.valueOf(i).pow(i));
        }
        String sum_str = sum.toString();
        System.out.println(sum_str.substring(sum_str.length()-10,sum_str.length()));
    }
}