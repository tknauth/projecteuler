package main;

public class Main {
	private static final int[] fac = {1, 1, 2, 6, 24, 120, 720, 7*720, 8*7*720, 9*8*7*720};
	
	// compute factorions -- brute force ...
	public static void main(String[] args) {
		int[] digits = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		int i;
		for (i=3; i<100000000 ; i++) {
			if (sum(i)==i) System.out.println(""+i);
		}
		System.out.println(""+(i-1)+" "+sum(i-1));
	}
	
	private static int sum(int[] ds) {
		int sum = 0;
		for (int d : ds) {
			sum += fac[d];
		}
		return sum;
	}
	private static int sum(int i) {
		char[] cs = Integer.toString(i).toCharArray();
		int[] ds = new int[cs.length];
		int ind = 0;
		for (char c : cs) {
			ds[ind] = c - '0';
			ind++;
		}
		return sum(ds);
	}
}
