package pe;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.LinkedList;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.io.BufferedReader;
import java.io.FileReader;

public class Pe58 {
  private static Set<Integer> primes = new HashSet<Integer>();

  public static void main(String[] args) {
    //readPrimes(primes, 1000);
    test();
    int elem = 1;
    int d = 0;                  // d + 1 == side length
    int cPrimes = 0;
    int cDiagElems = 1;
    float ratio = 0.0f;
    do {
      d += 2;
      for (int i=0; i<4; i++) {
        elem += d;
        if (mrPrimalityProven(elem)) {
          cPrimes++;
        }
      }
      cDiagElems += 4;
      ratio = ((float)cPrimes)/cDiagElems;
    } while (ratio >= 0.1);
    System.out.println(""+(d+1));
  }
  private static void test() {
    assert mrPrimalityProven(13921);
    assert mrPrimalityProven(99998749);
    assert mrPrimalityProven(99961723);
    assert false == mrPrimalityProven(99);
  }
  public static void readPrimes(Collection<Integer> c) {
    readPrimes(c, Integer.MAX_VALUE);
  }
  public static void readPrimes(Collection<Integer> c, int limit) {
    try {
      BufferedReader br = new BufferedReader(new FileReader("pe/primes.txt"));
      do {
        String line = br.readLine();
        if (null == line) break;
        Integer p = Integer.parseInt(line);
        if (p > limit) break;
        c.add(p);
      } while (true);
    } catch (Exception e) {e.printStackTrace();}
  }

  public static boolean mrPrimalityProven(long n) {
    if (n > 341550071728321L) {
      throw new UnsupportedOperationException("argument too big");
    }
    int[] trivial = {3, 5, 7, 11, 13, 17};
    for (int x : trivial) {
      if (n == x) return true;
    }
    if (n == 1 || (n % 2) == 0) return false;
    long d = n - 1;
    while ((d & 1) == 0) { d >>= 1; }
    
    for (int a : new int[] {2, 3, 5, 7, 11, 13, 17}) {
      long t = d;
      long y = BigInteger.valueOf(a).modPow(BigInteger.valueOf(t), BigInteger.valueOf(n)).longValue();
      while (t != n-1 && y != 1 && y != n-1) {
        y = (y * y) % n;
        t <<= 1;
      }
      if (y != n-1 && (t & 1) == 0) {
        return false;
      }
    }
    return true;
  }
}
