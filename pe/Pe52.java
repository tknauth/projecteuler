public class Pe52 {
  private static final int FACTORS = 6;
  public static void main(String[] args) {
    int n = 125874;
    test();
    do {
      if ((n % 10000) == 0) System.out.println(""+n+" ... ");
      if (Integer.toString(n).length() != Integer.toString(FACTORS*n).length()) {
        //System.out.print(""+n+"\t\t-> ");
        n = (int) Math.pow(10, Integer.toString(n).length());
        //System.out.println(""+n);
      }
      if (condition(n)) break;
      n++;
    } while (true);
    System.out.println(""+n);
  }
  public static boolean condition(int n) {
    int[] ns = new int[FACTORS];
    for (int i=0; i<FACTORS; i++) {
      ns[i] = n*(1+i);
      if (i>0) {
        if (false == sameDigits(ns[i-1], ns[i])) {
          return false;
        }
      }
    }
    if (n > 10000000) return true;
    return true;
  }

  public static boolean sameDigits(int n, int m) {
    return sameDigits(Integer.toString(n), Integer.toString(m));
  }

  public static boolean sameDigits(String n, String m) {
    boolean[] dn = new boolean[10], dm = new boolean[10];
    for (Character c : n.toCharArray()) {
      dn[c-'0'] = true;
    }
    for (Character c : m.toCharArray()) {
      dm[c-'0'] = true;
    }
    for (int i=0; i<dn.length; i++) {
      if (dn[i] != dm[i]) return false;
    }
    return true;
  }

  private static void test() {
    System.out.println(""+sameDigits(251748, 125874));
    System.out.println(""+sameDigits(251745, 125874));
  }
}
