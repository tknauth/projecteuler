package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;

public class Pe74 {

    static final int factorial[] = new int[]{1,1,2,6,24,120,720,5040,40320,362880};
    static final int N = 1000000;
    // 2177280 = 6*factorial(9) is the largest integer we would expect
    // to encounter for inputs below one million
    static int a[] = new int[2177280+1];

    public static void main(String[] args) {
        test();

        int result = 0;
        for (int i=0; i<N; ++i) {
            // If chain length unknown
            if (a[i] == 0) {
                List<Integer> steps = new LinkedList<Integer>();
                Set<Integer> elements = new HashSet<Integer>();
                int x = i;
                // Loop until we encounter a loop in the chain, or we
                // hit a number we already know the chain length for
                do {
                    steps.add(x);
                    elements.add(x);
                    x = digitalFactorizedSum(x);
                    // System.out.print(x+" ");
                } while (!elements.contains(x) && (a[x] == 0));
                // System.out.println("");

                int n = 0;
                for (int step : steps) {
                    if (step == x) break;
                    assert a[step] == 0;
                    // If we found a new chain with all elements
                    // unknown, a[x] will be 0. Otherwise, a[x] will
                    // be the additional chain length from where we
                    // stop in the previous loop.
                    a[step] = steps.size() - n + a[x];
                    // System.out.println("a["+step+"]= "+a[step]);
                    if (a[step] == 60) ++result;
                    ++n;
                }
                // System.out.println("steps= "+steps);
            }
        }
        System.out.println(""+result);
    }

    private static void test() {
    }

    private static int digitalFactorizedSum(int n) {
        int result = 0;
        while (n > 0) {
            result += factorial[n % 10];
            n /= 10;
        }
        return result;
    }
}
