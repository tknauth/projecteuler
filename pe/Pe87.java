package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;
import pe.Pe58;

// Simply enumerate all possible solutions. There are only 7071, 368,
// and 84 possibilities for x, y, and z respectively (including
// non-primes) and even less, if only considering prime numbers. From
// all possible solutions we discard those > 50*10^6.

public class Pe87 {

    public static void main(String[] args) {
        test();
        final long V = 50*1000*1000;
        List<Long> Xs = new ArrayList<Long>();
        List<Long> Ys = new ArrayList<Long>();
        List<Long> Zs = new ArrayList<Long>();

        Xs.add(2L);
        for (int i=3; i<=Math.floor(Math.pow(V, 1.0/2)); i+=2) {
            if (Pe58.mrPrimalityProven(i)) Xs.add((long)i);
        }
        Ys.add(2L);
        for (int i=3; i<=Math.floor(Math.pow(V, 1.0/3)); i+=2) {
            if (Pe58.mrPrimalityProven(i)) Ys.add((long)i);
        }
        Zs.add(2L);
        for (int i=3; i<=Math.floor(Math.pow(V, 1.0/4)); i+=2) {
            if (Pe58.mrPrimalityProven(i)) Zs.add((long)i);
        }

        Set<Long> a = new HashSet<Long>();
        for (Long x : Xs) {
            for (Long y : Ys) {
                for (Long z : Zs) {
                    long l = x*x+y*y*y+z*z*z*z;
                    if (l < V) a.add(l);
                }
            }
        }
        System.out.println(""+a.size());
    }

    private static void test() {
    }
}
