package main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {
	public static void main(String[] args) {
		try {
			BufferedReader br = new BufferedReader(new FileReader("./words.txt"));
			String line = br.readLine();
			if (null == line) return;
			String[] words = line.split(",");
			int[] wvs = new int[words.length]; // wv = word value
			int max_wv = 0;
			for (int i=0; i<words.length; i++) {
				words[i] = new StringBuffer(words[i]).deleteCharAt(0).deleteCharAt(words[i].length()-2).toString();
				int wv = 0;
				for (char c : words[i].toCharArray()) {
					wv += c-'A'+1;
				}
				wvs[i] = wv;
				if (wv > max_wv) max_wv = wv;
			}
			// compute triangle nr up to max
			Set<Integer> tri_nums = triangleNumbers(max_wv);
			int ntv = 0; // # triangle values
			for (int wv : wvs) {
				if (tri_nums.contains(wv)) ntv++;
			}
			System.out.println(""+ntv);
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
	}
	private static Set<Integer> triangleNumbers(int limit) {
		Set<Integer> res = new HashSet<Integer>();
		int n=1;
		do {
			int t_n = n*(n+1)/2;
			if (t_n >= limit) return res;
			res.add(t_n);
			n++;
		} while (true);
	}
}
