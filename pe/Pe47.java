package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;

import pe.Pe58;

public class Pe47 {
  private static List<Integer> primes = new ArrayList<Integer>();
  public static void main(String[] args) {
    Pe58.readPrimes(primes, 100000);
    test();
    // pfs = prime factors
    final int N = 4;
    List<List<Integer>> pfs = new ArrayList<List<Integer>>();
    for (int i=0; i<N; i++) pfs.add(new ArrayList<Integer>());
    int i=0;
    do {
      i++;
      
      Set<Integer> all_pfs = new HashSet<Integer>();
      pfs.set(i%N, getpf(i));
      if (pfs.get(i%N).size() < N) {
        i+=3;
        for (int j=0; j<N; j++) {
          pfs.set(j, new ArrayList<Integer>());
        }
        continue;
      }

//       System.err.println("i= "+i);
//       for (List<Integer> pf : pfs) {
//         System.err.println(Arrays.toString(pf.toArray()));
//       }

      all_pfs.addAll(pfs.get((i+1)%N));
      all_pfs.addAll(pfs.get((i+2)%N));
      if (all_pfs.size() != 2*N) continue;
      all_pfs.clear();
      all_pfs.addAll(pfs.get((i+2)%N));
      all_pfs.addAll(pfs.get((i+3)%N));
      if (all_pfs.size() != 2*N) continue;
      all_pfs.clear();
      all_pfs.addAll(pfs.get((i+3)%N));
      all_pfs.addAll(pfs.get((i+4)%N));
      if (all_pfs.size() != 2*N) continue;

      System.out.println(""+(i-N+1));
      break;
      
    } while(true);
  }

  private static void test() {
    assert Arrays.equals(getpf(644).toArray(), new Integer[] {2, 7, 23});
    assert Arrays.equals(getpf(645).toArray(), new Integer[] {3, 5, 43});
    assert Arrays.equals(getpf(646).toArray(), new Integer[] {2, 17, 19});
  }

  public static List<Integer> getpf(int x) {
    List<Integer> pfs = new ArrayList<Integer>();
    if (0 == x) return pfs;
    for (int p : primes) {
//       if (primes.contains(x)) {
      if (Pe58.mrPrimalityProven(x)) {
        pfs.add(x);
        break;
      }
      int x_div = x / p;
      if (x_div*p == x) {
        pfs.add(p);
        // not interested if the same prime factor appears
        // multiple times
        do {
          x = x_div;
          x_div = x / p;
        } while (x_div * p == x);
      }
      if (1==x) break;
    }
    return pfs;
  }
}
