package main;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

public class Main {
	private static final int LIMIT = 100;
	public static void main(String[] args) {
		Set<BigInteger> unique = new HashSet<BigInteger>();
		for (int a=2; a<=LIMIT; a++) {
			for (int b=2; b<=LIMIT; b++) {
				unique.add(BigInteger.valueOf(a).pow(b));
			}
		}
		System.out.println(""+unique.size());
	}
}
