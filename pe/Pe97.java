package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;

public class Pe97 {
  public static void main(String[] args) {
    test();
    BigInteger bi = BigInteger.valueOf(2).modPow(BigInteger.valueOf(7830457), BigInteger.valueOf(10000000000L));
    bi = bi.multiply(BigInteger.valueOf(28433));
    bi = bi.add(BigInteger.ONE);
    System.out.println(""+bi.toString().substring(bi.toString().length()-10, bi.toString().length()));
  }
  private static void test() {
  }
}
