from random import choice
from itertools import combinations

def gen(operands, operators, n) :
    result = []
    
    if len(operands) > 0 :
        for operand in operands :
            new_operands = list(operands)
            new_operands.remove(operand)
            result += map(lambda s : operand + s,
                          gen(new_operands,
                              list(operators),
                              n+1))

    if n >= 2 :
        for operator in operators :
            # new_operators = list(operators)
            # new_operators.remove(operator)
            result += map(lambda s : operator + s,
                          gen(list(operands),
                              operators,
                              n-1))

    if len(result) == 0 : return ['']

    return result

def eval_postfix(expr) :
    stack = []
    
    for c in expr :
        if c in set('+-*/') :
            assert len(stack) >= 2
            op1 = str(stack.pop())
            op2 = str(stack.pop())
            try :
                stack.append(eval(op1+c+op2))
            except ZeroDivisionError :
                return float('nan')                
        else :
            stack.append(float(c))

    assert(len(stack) == 1)
    return stack.pop()

def consecutive(xs) :
    i = 1
    for x in xs :
        if i != x : return i-1
        i += 1
    

assert eval_postfix('2413+*/') == 8.0
assert eval_postfix('4321/+*') == 14.0
assert eval_postfix('1423+*-') == 19.0
assert eval_postfix('3412+**') == 36.0

def is_integral(x) :
    return x==int(x)

max_consecutive = 0

for operands in combinations(range(1,10), 4) :
    l = gen(list('%d%d%d%d'%operands), list('+-*/'), 0)

    vs = map(eval_postfix, l)
    vs = filter(lambda x : (x >= 1.) and (is_integral(x)), vs)
    consec = consecutive(sorted(map(int, set(vs))))
    if consec > max_consecutive :
        print operands, consec
        max_consecutive = consec
