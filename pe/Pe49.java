import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Set;
import java.util.Arrays;
import java.util.List;
import java.util.LinkedList;
import java.util.Collections;

public class Pe49 {
    public static void main(String[] args) {
        Set<Integer> primes = new HashSet<Integer>();
        try {
            BufferedReader br = new BufferedReader(new FileReader("10000.txt"));
            do {
                String line = br.readLine();
                if (null == line) break;
                String[] primes_str = line.trim().split("\\s+");
                for (String p_str : primes_str) {
                    //System.out.println(p_str);
                    Integer p = Integer.parseInt(p_str);
                    if (p >= 1000 && p <= 9999) primes.add(p);
                }
            } while (true);
        } catch (Exception e) {e.printStackTrace();}
        Set<Integer> wc = new HashSet<Integer>(primes);
        do {
            int p = wc.iterator().next();
            String[] perms = getPermutations(p);
            List<Integer> cands = new LinkedList<Integer>();
            for (int i=0; i<perms.length; i++) {
                if (primes.contains(Integer.parseInt(perms[i]))) {
                    wc.remove(Integer.parseInt(perms[i]));
                    cands.add(Integer.parseInt(perms[i]));
                }
            }
            Collections.sort(cands);
            if (cands.size()>2) {
                //System.out.print(p+" : ");
                //System.out.println(Arrays.toString(cands.toArray()));
                int prev = 0;
                int d_prev = 0;
                for (Integer cand : cands) {
                    if (0 != prev) {
                        if (d_prev == cand - prev) {
                            System.out.println(prev+" :::: "+cand);
                            System.out.println(Arrays.toString(cands.toArray()));
                        }
                        //System.out.print((cand - prev)+", ");
                        d_prev = cand - prev;
                    }
                    prev = cand;
                }
                //System.out.println("");
            }
            wc.remove(p);
        } while (!wc.isEmpty());
        //System.out.println(""+primes.toString());
        //test();
    }

    private static String[] getShifts(int n) {
        String[] res = new String[Integer.toString(n).length()];
        res[0] = Integer.toString(n);
        for (int i=1; i<res.length; i++) {
            res[i] = new String(res[i-1]);
            res[i] = new StringBuffer(res[i]).insert(res[i].length(), res[i].charAt(0)).deleteCharAt(0).toString();
        }
        return res;
    }

    private static void test() {
        //System.out.println(Arrays.toString(getShifts(4323)));
        System.out.println(fac(4));
        System.out.println(Arrays.toString(getPermutations(3677)));
        System.out.println(getPermutation(0, "3677"));
    }

    private static String[] getPermutations(int n) {
        Set<String> res = new HashSet<String>();
        String n_str = Integer.toString(n);
        for (int i=0; i<fac(n_str.length()); i++) {
            res.add(getPermutation(i, n_str));
        }
        //System.out.println(res.size());
        return res.toArray(new String[1]);
   }

    private static String getPermutation(int k, String s) {
//         function permutation(k, s) {
//             for j = 2 to length(s) {
//                     swap s[(k mod j) + 1] with s[j]; // note that our array is indexed starting at 1
//                     k := k / j;        // integer division cuts off the remainder
//                 }
//             return s;
//         }
        StringBuffer res = new StringBuffer(s);
        for (int j=2; j<=s.length(); j++) {
            char tmp = res.charAt(j-1);
            res.setCharAt(j-1, res.charAt(k % j));
            res.setCharAt(k%j, tmp);
            k /= j;
        }
        //System.out.println("lala "+res.toString());
        return res.toString();
    }

    private static int fac(int n) {
        int res = 1;
        for (int i=0; i<n; i++) res *= (n-i);
        return res;
    }
}
