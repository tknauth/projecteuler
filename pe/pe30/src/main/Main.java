package main;

import java.util.Set;

public class Main {
	private static final int LIMIT = 10000000;
	private static final int[] powers = {0, 1,
		2*2*2*2*2,
		3*3*3*3*3,
		4*4*4*4*4,
		5*5*5*5*5,
		6*6*6*6*6,
		7*7*7*7*7,
		8*8*8*8*8,
		9*9*9*9*9
	};
	public static void main(String[] args) {
		int sum = 0;
		for (int i=2; i<LIMIT; i++) {
			String itos = Integer.toString(i);
			int digitSum = 0;
			for (int digits=0; digits<itos.length(); digits++) {
				digitSum += powers[itos.charAt(digits)-'0']; 
			}
			if (digitSum == i) {
				System.out.println(""+i);
				sum += i;
			}
		}
		System.out.println(""+sum);
	}
}
