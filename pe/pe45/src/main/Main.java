package main;

public class Main {

	public static void main(String[] args) {
		int n=1;
		int cSol=0;
		do {
			long tri = n*(n+1L)/2L;
			if (isPentNr(tri) && isHex(tri)) {
				System.out.println(""+tri);
				cSol++;
			}
			n++;
		} while(cSol<3);
	}
	private static boolean isPentNr(long n) {
			long x = ((long)Math.sqrt(24*n+1L)+1L)/6L;
			return x*(3L*x-1L)/2L==n;
	}
	private static boolean isTri(long x) {
		long sqrt = (long)Math.sqrt(8*x+1);
		return sqrt*sqrt==x;
	}
	private static boolean isHex(long x) {
		long n = ((long)Math.sqrt(8L*x+1L)+1L)/4L;
		return n*(2*n-1) == x;
	}
}
