#include <stdio.h>
#include <math.h>

/* Tried many wrong approaches. I had high hopes with Wikipedia, which
   turned out to be wrong:
   http://en.wikipedia.org/wiki/Integer_triangle#Heronian_triangles
   Enumerating u and v does not yield all isosceles Heronian
   triangles. For example, the triangle with side lengths 16, 17, 17
   does not follow from the above equation.

   Finally, enumerating all side lengths and checking if the area is
   integral was hampered by the very large numbers following from the
   general area formula sqrt(s*(s-a)(s-b)(s-c)). The less general area
   formula for isoscele triangles allows to correctly compute in
   64-bit signed integers. */

int static is_perfect_square(long long x) {
    long long square_root = (long long) floor(sqrt(x)+0.5);
    return (square_root * square_root == x);
}

int main(int argc, char* argv[]) {
    long a = 5;
    long c = a + 1;
    long long sum = 0;

    do {
        long long sq = 4*a*a-c*c;

        if (is_perfect_square(sq)) {
            printf("%ld %lld\n", a, sq);
            sum += (a+a+c);
        }

        sq = 4*a*a-(c-2)*(c-2);
        if (is_perfect_square(sq)) {
            sum += (a+a+c-2);
        }

        a += 2;
        c += 2;
    } while (a+a+c <= 1000000000L);

    printf("%lld\n", sum);

    return 0;
}
