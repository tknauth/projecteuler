package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;

public class Pe99 {
  private static List<Integer> primes = new ArrayList<Integer>();
  public static void main(String[] args) {
    double maxval = -1;
    int resultLine = -1;
    try {
      BufferedReader br = new BufferedReader(new FileReader("pe/base_exp.txt"));
      int cLine = 1;
      do {
        String line = br.readLine();
        if (null == line) break;
        String[] temp = line.trim().split(",");
        assert temp.length == 2;
        int base = Integer.parseInt(temp[0]);
        int exp  = Integer.parseInt(temp[1]);
        double val = exp * Math.log(base);
        if (val > maxval) {
//           System.err.println(""+cLine+", "+val+", "+Math.log(base)+", "+Math.log(exp));
          resultLine = cLine;
          maxval = val;
        }
        cLine++;
      } while (true);
    } catch (Exception e) {e.printStackTrace();}
    System.out.println(""+resultLine);
  }

  private static void test() {
  }
}
