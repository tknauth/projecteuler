package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;
import pe.Common;

public class Pe75 {

    private static ArrayList<Integer> primes = new ArrayList<Integer>();

    public static void main(String[] args) {
        test();
    
        int N = 1500000;
        if (args.length > 0) {
            N = Integer.parseInt(args[0]);
        }

        primes.add(2);
        for (int n=3; n<=N; n+=2) {
            if (Pe58.mrPrimalityProven(n)) primes.add(n);
        }

        int x[] = new int[N+1];

        for (int m=2; m*m<=N/2; ++m) {
        // for (int m=2; 2*m*m+2*m<=N; ++m) {
            // System.err.println(""+m);
            for (int n=m-1; n>0; --n) {

                assert m>n;

                int a = m*m-n*n;
                int b = 2*m*n;
                final int c = m*m+n*n;

                if (a>b) {
                    int tmp = a;
                    a = b;
                    b = tmp;
                }
                // System.err.println(""+a+" "+b+" "+m+" "+n);
                assert a<b;
                assert b<c;

                // System.err.println(a+" "+b+" "+c);
                if (a+b+c>N) continue;

                if (Common.isPrimitivePythagorean(m, n)) {
                    // System.err.println(a+" "+b+" "+c);
                    for (int k=1; k*(a+b+c)<=N; ++k) {
                        // System.err.println(k*a+" "+k*b+" "+k*c);
                        x[k*(a+b+c)] += 1;
                    }
                }
            }
        }

        int result = 0;
        for (int i=0; i<x.length; ++i) {
            if (x[i] == 1) {
                // System.err.println(""+i+" : "+x[i]);
                ++result;
            }
        }
        System.out.println(""+result);
    }

    private static void test() {
    }
}
