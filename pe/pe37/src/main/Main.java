package main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

public class Main {
	private static Set<Integer> primes = new TreeSet<Integer>();
	private static final int LIMIT = 1000000;
	public static void main(String[] args) {
		testing();
		try {
			BufferedReader br = new BufferedReader(new FileReader("/home/thomas/playground/python/primes.db"));
			int p;
			do {
				String line = br.readLine();
				if (null == line) break;
				p = Integer.parseInt(line);
				primes.add(p);
			} while(p<LIMIT);
		} catch (FileNotFoundException e) {
			
		} catch (IOException e) {
			
		}
		
		int sum = 0;
		for (int p : primes) {
			int[] shifts = shifts(p);
			boolean all_prime = true;
			for (int shift : shifts) {
				all_prime &= primes.contains(shift);
			}
			if (all_prime && p>10) {
				System.out.println(""+p);
				sum += p;
			}
		}
		System.out.println(""+sum);
	}
	private static int[] shifts(int n) {
		StringBuffer n_str = new StringBuffer(Integer.toString(n));
		StringBuffer n_str2 = new StringBuffer(Integer.toString(n));
		int[] shifts = new int[(n_str.length()-1)*2];
		for (int i=0; i<shifts.length/2; i++) {
			n_str.deleteCharAt(0);
			n_str2.deleteCharAt(n_str2.length()-1);
			shifts[i] = Integer.parseInt(n_str.toString());
			shifts[shifts.length-i-1] = Integer.parseInt(n_str2.toString());
		}
		return shifts;
	}
	private static void testing() {
		int[] temp = shifts(42);
		System.out.println(temp.toString());
	}
}
