import java.math.BigInteger;

public class Pe53 {
  private static final int SIZE = 101;
  private static final BigInteger[] factorial;
  static {
    factorial = new BigInteger[SIZE];
    factorial[0] = BigInteger.ONE;
    for (int i=1; i<SIZE; i++) {
      factorial[i] = factorial[i-1].multiply(BigInteger.valueOf(i));
    }
  }
  public static void main(String[] args) {
    int c = 0;
    test();
    for (int n=1; n<=100; n++) {
      for (int r=0; r<=n; r++) {
        if (factorial[n].divide(factorial[r].multiply(factorial[n-r])).compareTo(BigInteger.valueOf(1000000)) == 1) {
          c++;
        }
      }
    }
    System.out.println(""+c);
  }
  private static void test() {
    System.out.println(""+factorial[23].divide(factorial[10].multiply(factorial[23-10])).intValue());
    System.out.println("");
  }
}
