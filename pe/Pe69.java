package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;
import pe.Pe58;

public class Pe69 {

    static final int N = 1000000;

    static TreeSet<Integer> primes = new TreeSet<Integer>();

    public static void main(String[] args) {
        test();

        int result_n = 2;
        double result_n_over_phi_for_n = 2.0;

        //Pe58.readPrimes(primes, N);
        primes.add(2);
        for (int n=3; n<=N; n+=2) {
            if (Pe58.mrPrimalityProven(n)) primes.add(n);
        }
        for (int n=2 ; n<=N; ++n) {
            HashMap<Integer, Integer> factor = new HashMap<Integer, Integer>();
            int tmp = n;
            while (false == primes.contains(tmp)) {
                for (int p : primes) {
                    if (((int)Math.floor((tmp/(double)p)))*p == tmp) {
                        Integer multiplicity = factor.get(p);
                        if (null == multiplicity) {
                            factor.put(p, 1);
                        } else {
                            factor.put(p, multiplicity+1);
                        }
                        tmp /= p;
                        break;
                    }
                }
            }
            Integer multiplicity = factor.get(tmp);
            if (null == multiplicity) {
                factor.put(tmp, 1);
            } else {
                factor.put(tmp, multiplicity+1);
            }
            int phi = 1;
            for (Map.Entry<Integer, Integer> x : factor.entrySet()) {
                phi *= (x.getKey()-1)*(int)Math.pow(x.getKey(), x.getValue()-1);
            }
            //System.out.println(""+n+"\t\t"+phi);
            if (n/(double)phi > result_n_over_phi_for_n) {
                result_n_over_phi_for_n = n/(double)phi;
                result_n = n;
            }
        }
        System.out.println(""+result_n);
    }

    private static void test() {
    }
}
