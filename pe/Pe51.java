package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;

import pe.Pe58;

public class Pe51 {
  public static void main(String[] args) {
    test();
    Set<Integer> primes = new HashSet<Integer>();
    Pe58.readPrimes(primes, 1000000);
    Set<Integer> work = new HashSet<Integer>();
    for (int prime : primes) {
      if (prime > 56773) work.add(prime);
    }
    while (!work.isEmpty()) {
//       System.err.println(work.size()+" primes left");
      int prime = work.iterator().next();
      work.remove(new Integer(prime));
      final int digits = Integer.toString(prime).length();
      for (int replace=1; replace<digits; replace++) {
        List<int[]> patterns = combinations(digits-1, replace);
        for (int[] pattern : patterns) {
          int cNotPrime = 0;
          int smallest = Integer.MAX_VALUE;
          for (int x=0; x<10; x++) {
            StringBuffer cand = new StringBuffer(Integer.toString(prime));
            for (int i : pattern) {
              cand.setCharAt(i, (char)(x+'0'));
            }
            int cand_int = Integer.parseInt(cand.toString());
//             if (!primes.contains(cand_int)) {
            if (!Pe58.mrPrimalityProven(cand_int)) {
              cNotPrime++;
            } else {
              work.remove(new Integer(cand_int));
              smallest = Math.min(smallest, cand_int);
            }
            if (cNotPrime > 3) break;
          }
          if (cNotPrime <=2 ) {
            System.err.println("pattern= "+Arrays.toString(pattern)+", prime= "+prime+", with "+cNotPrime+" non-primes, smallest= "+smallest);
          }
        }
      }
    }
    System.out.println("");
  }
  private static void test() {
//     Integer[] a = {1,2,3,4,5};
//     List<Integer[]> r = combinations(a, 4);
//     System.err.println(""+Arrays.toString(r.toArray()));
//     combinations(4,2);
//     System.err.println("");
//     combinations(5,2);
//     System.err.println("");
//     combinations(5,3);
  }

  /**
   * @param n total number of elements in set
   * @param k number of elements to draw
   * @return All possible combination of element indices
   */
  public static List<int[]> combinations(int n, int k) {
    List<int[]> result = new LinkedList<int[]>();
    int[] p = new int[k];
    for (int i=0; i<k; i++) p[i] = i;
    do {
      result.add(p.clone());
//       System.err.println(Arrays.toString(p));

      for (int i=1; i<=k; i++) {
        // find left-most pointer to move one up
        // i==k will be true, if the right-most pointer needs to be moved
        if (i==k || p[i-1]+1 < p[i]) {
          p[i-1]++;
          for (int j=0; j<i-1; j++) p[j] = j;
          break;
        }
      }

      if (p[k-1]==n) break;
    } while (true);
    return result;
  }
}
