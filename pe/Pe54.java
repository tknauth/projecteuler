package pe;

import java.math.BigInteger;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;

public class Pe54 {
  public static void main(String[] args) {
    test();
    int c = 0;
    try {
      BufferedReader br = new BufferedReader(new FileReader("pe/poker.txt"));
      String line = br.readLine();
      while (null != line) {
        int[] p1_cards = new int[13], p2_cards = new int[13];
        int[] p1_suits = new int[4], p2_suits = new int[4];
        String[] cards = line.split(" ");
        for (int i=0; i<5; i++) {
          p1_cards[c(cards[i].charAt(0))]++;
          p1_suits[s(cards[i].charAt(1))]++;
        }
//         System.out.println(line);
//         System.out.println(Arrays.toString(p1_cards));
//         System.out.println(Arrays.toString(p1_suits));
        for (int i=5; i<10; i++) {
          p2_cards[c(cards[i].charAt(0))]++;
          p2_suits[s(cards[i].charAt(1))]++;
        }
//         System.out.println(Arrays.toString(p2_cards));
//         System.out.println(Arrays.toString(p2_suits));
        if (hand(p1_cards, p1_suits) > hand(p2_cards, p2_suits)) {
          c++;
        } else if (hand(p1_cards, p1_suits) == hand(p2_cards, p2_suits)) {
          System.out.println("tie breaker neeeded! "+line);
        }
        line = br.readLine();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    System.out.println(""+c);
  }

  public static int hand(int[] cards, int[] suits) {
    if (flush(suits) && straight(cards) && (1 == cards[12])) return 0;
    if (flush(suits) && straight(cards)) return -1;
    if (isFourOfKind(cards)) return -50;
    if (isFullHouse(cards)) return -100;
    if (flush(suits)) return -150;
    if (straight(cards)) return -200;
    if (isThreeOfKind(cards)) return -250;
    if (isTwoPairs(cards)) return -300;
    if (isPair(cards) >= 0) return -400+isPair(cards);
    return -450-13+highestCard(cards);
  }

  private static void test() {
    int[] cards = {0,0,1,1,1,1,1,0,0,0,0,0,0};
    int[] suits = {5,0,0,0};
    assert straight(cards);
    assert flush(suits);
    cards = new int[] {0,0,1,1,1,1,0,1,0,0,0,0,0};
    suits = new int[] {0,2,3,0};
    assert false == straight(cards);
    assert false ==  flush(suits);
    cards = new int[] {0,0,0,0,0,0,0,0,1,1,1,1,1};
    suits = new int[] {0,5,0,0};
//     System.out.println(flush(suits));
//     System.out.println(straight(cards));
    assert 0 == hand(cards, suits);
    cards = new int[] {0,0,0,0,0,0,1,1,1,1,1,0,0};
    suits = new int[] {0,5,0,0};
    assert (-1 == hand(cards, suits));
  }

  private static int c(char card) {
    if (card >= '2' && card <= '9') return card-'0'-2;
    if (card == 'T') return 8;
    if (card == 'J') return 9;
    if (card == 'Q') return 10;
    if (card == 'K') return 11;
    if (card == 'A') return 12;
    throw new RuntimeException();
  }

  private static int s(char suit) {
    if (suit == 'C') return 0;
    if (suit == 'D') return 1;
    if (suit == 'H') return 2;
    if (suit == 'S') return 3;
    throw new RuntimeException();
  }

  public static boolean straight(int[] cards) {
    for (int i=0; i<=cards.length-5; i++) {
      boolean anded = true;
      for (int n=0; n<5; n++) {
        anded &= (1 == cards[i+n]);
      }
      if (anded) return true;
    }
    return false;
 }

  public static boolean flush(int[] suits) {
    for (int s : suits) {
      if (5 == s) return true;
    }
    return false;
  }

  public static boolean isFourOfKind(int[] cards) {
    for (int s : cards) {
      if (4 == s) return true;
    }
    return false;
  }

  public static boolean isFullHouse(int[] cards) {
    boolean threeOfKind = false, pair = false;
    for (int c : cards) {
      if (3 == c) threeOfKind = true;
      if (2 == c) pair = true;
    }
    return threeOfKind && pair;
  }

  public static boolean isThreeOfKind(int[] cards) {
    for (int c : cards) {
      if (3 == c) return true;
    }
    return false;
  }

  public static boolean isTwoPairs(int[] cards) {
    int pairs = 0;
    for (int c : cards) {
      if (2 == c) pairs++;
    }
    return 2 == pairs;
  }

  public static int isPair(int[] cards) {
    for (int i=0; i<cards.length; i++) {
      if (2 == cards[i]) return i;
    }
    return -1;
  }

  public static int highestCard(int[] cards) {
    for (int i=cards.length-1; i>=0; i--) {
      if (1 == cards[i]) return i+1;
    }
    throw new RuntimeException();
  }
}
