package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;

public class Pe68 {

    public static BigInteger foo(Object[] o) {
        int lowest = 0;
        for (int i=0; i<o.length; ++i) {
            if (((int[])o[i])[0] < ((int[])o[lowest])[0]) lowest = i;
        }
        StringBuffer sb = new StringBuffer();
        for (int i=0; i<o.length; ++i) {
            int[] x = (int[])o[(lowest+i)%o.length];
            sb.append(""+x[0]+x[1]+x[2]);
        }
        return new BigInteger(sb.toString());
    }

    /** Only interested in 16 digit strings -> inner circle variables
     * (b, d, f, h, i in our case) cannot be 10. Rest is finding the
     * result we're interested in. */
    public static void main(String[] args) {
        test();
        BigInteger result = BigInteger.ZERO;
        for (int a=1; a<=10; ++a) {
            for (int b=1; b<10; ++b) {
                if (b == a) continue;
                for (int d=1; d<10; ++d) {
                    if (d==a || d==b) continue;
                    int x = a + b + d;
                    for (int c=1; c<=10; ++c) {
                        if (c==a || c==b || c==d) continue;
                        int f = x - c - d;
                        if (f <= 0 || f >= 10) continue;
                        if (f == a ||
                            f == b ||
                            f == c ||
                            f == d) continue;
                        for (int e=1; e<=10; ++e) {
                            if (e==a || e==b || e==c || e==d || e==f) continue;
                            int h = x - f - e;
                            if (h <= 0 || h >= 10) continue;
                            if (h==a || h==b || h==c || h==d || h==f || h==e) continue;
                            for (int g=1; g<=10; ++g) {
                                if (g==a || g==b || g==c || g==d || g==e || g==f || g==h) continue;
                                int i = x - h - g;
                                if (i <= 0 || i >= 10) continue;
                                if (i==a || i==b || i==c || i==d || i==e || i==f || i==h || i==g) continue;
                                int k = x - i - b;
                                if (k <= 0 || k > 10) continue;
                                if (k==a || k==b || k==c || k==d || k==e || k==f || k==h || k==g || k==i) continue;
                                Object[] o = new Object[] {new int[] {a,b,d},
                                                           new int[] {c,d,f},
                                                           new int[] {e,f,h},
                                                           new int[] {g,h,i},
                                                           new int[] {k,i,b}};
                                BigInteger cand_result = foo(o);
                                if (1 == cand_result.compareTo(result)) result = cand_result;
                            }
                        }
                    }
                }
            }
        }
        System.out.println(""+result.toString());
    }
    private static void test() {
    }
}
