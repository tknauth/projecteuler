package main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Main {

	private static final int DIM = 100;
	public static int[][] input = new int[DIM][DIM];
	public static int[][] output = new int[DIM][DIM];
	private static void readInput() {
		try {
			BufferedReader br = new BufferedReader(new FileReader("pe67.txt"));
			int row = 0;
			do {
				int col = 0;
				String s = br.readLine();
				if (null == s) break;
				Arrays.fill(input[row], 0);
				String[] split = s.split(" ");
				for (String nr : split) {
					input[row][col] = Integer.parseInt(nr);
					col++;
				}
				row++;
			} while (true);
		} catch (FileNotFoundException e) {
			
		} catch (IOException e) {
			
		}
	}
	
	private static void reformatInput() {
		int temp[][] = new int[DIM][DIM];
		for (int i=0; i<DIM; i++) {
			for (int j=0; j<DIM; j++) {
				if (i+j<DIM) {
					temp[i][j] = input[i+j][j];
				} else {
					break;
				}
			}
		}
		input = temp;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		readInput();
		reformatInput();
		System.out.println("lalal");
		int result = 0;
		for (int i=0; i<DIM; i++) {
			for (int row=i, col=0; row>=0; row--, col++) {
				System.out.println("("+row+","+col+")");
				int a = row >= 1 ? input[row-1][col] : 0;
				int b = col >= 1 ? input[row][col-1] : 0;
				input[row][col] += Math.max(a, b);
				result = Math.max(result, input[row][col]);
			}
			System.out.println("-----");
		}
		System.out.println("result= "+result);
	}
}
