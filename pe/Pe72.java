package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;
import pe.Pe58;
import pe.Pe70;

public class Pe72 {

    public static void main(String[] args) {
        test();

        final int d_limit = args.length > 0 ? Integer.parseInt(args[0]) : 1000000;
        List<Integer> primes = new ArrayList<Integer>();
        primes.add(2);
        long begin = System.currentTimeMillis();
        System.err.println(begin+" start calc primes <="+d_limit);
        for (int n=3; n<=d_limit; n+=2) {
            if (Pe58.mrPrimalityProven(n)) primes.add(n);
        }
        long end = System.currentTimeMillis();
        System.err.println(end+" end calc primes");
        System.err.println("took "+(end-begin)+"ms = "+(end-begin)/1000.0+"s");
        
        long s = 0;
        for (int d=2; d<=d_limit; ++d) {
            // if (Pe58.mrPrimalityProven(d)) {
            if (Collections.binarySearch(primes, Integer.valueOf(d)) >= 0) {
                // s = s.add(BigInteger.valueOf(d-1));
                s += d-1;
            } else {
                // s = s.add(BigInteger.valueOf(Pe70.phi(d, primes.iterator())));
                s += Pe70.phi(d, primes);
            }
        }
        System.out.println(""+s);
    }

    public static int gcd(int a, int b) {
        if (b > a) {
            int tmp = a;
            a = b;
            b = tmp;
        }
        assert a >= b;

        do {
            int r = a - ((a / b) * b);
            if (r == 0) return b;
            a = b;
            b = r;
        } while (a >= b);

        return 1;
    }

    private static void test() {
        assert gcd(8,3) == 1;
        assert gcd(8,4) == 4;
        assert gcd(4,8) == 4;
        assert gcd(4,16) == 4;
        assert gcd(1071,462) == 21;
    }
}
