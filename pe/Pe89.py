#!/usr/bin/env python

roman_numeral2arabic = {'M' : 1000, 'D' : 500, 'C' : 100, 'L' : 50, 'X' : 10, 'V' : 5, 'I' : 1}
subtractive_arabic2roman = {900 : 'CM', 400 : 'CD', 90 : 'XC', 40 : 'XL', 9 : 'IX', 4 : 'IV'}
subtractive_roman2arabic = dict((v,k) for k,v in subtractive_arabic2roman.iteritems())

def is_substractive_pair(s) :
    assert len(s) == 2
    return (s in subtractive_roman2arabic.keys())

def roman2arabic(roman_numeral) :
    res = 0
    prev_c = None
    skip = False
    for (i,c) in enumerate(roman_numeral) :
        if skip :
            skip = False
            continue
        
        if i+2 <= len(roman_numeral) and is_substractive_pair(roman_numeral[i]+roman_numeral[i+1]) :
            res += subtractive_roman2arabic[roman_numeral[i]+roman_numeral[i+1]]
            skip = True
        else :
            res += roman_numeral2arabic[c]
    return res

def synth_roman_numeral(n) :
    arabic2roman = {1000 : 'M', 500 : 'D', 100 : 'C', 50 : 'L', 10 : 'X', 5 : 'V', 1 : 'I'}
    all_numerals = subtractive_arabic2roman.items() + arabic2roman.items()
    all_numerals = sorted(all_numerals, key=lambda t : t[0], reverse=True)
    res = ''
    while n > 0 :
        for numeral in all_numerals :
            if n >= numeral[0] :
                n -= numeral[0]
                res += numeral[1]
                break
    return res

# generative vs reductionist approach? generativ: determine n and
# re-build from scratch. reductionist takes Roman representation and
# tries to reduce it. which is easier?

# generative approach: construct all Roman numerals in minimal form up
# to 10k?; do conversion (Roman to Arabic) and lookup for each numeral
# in input.

# letters are only saved if subtractive pair can be applied

letters_saved = 0
for line in open('/Users/thomas/projecteuler/p89-input.txt') :
    n = line.strip()
    a = roman2arabic(n)
    r = synth_roman_numeral(a)
    assert len(r) <= len(n)
    letters_saved += len(n) - len(r)

print letters_saved
# print synth_roman_numeral(100)
# print synth_roman_numeral(1606)
# print synth_roman_numeral(4672)

assert (roman2arabic('MCCCCCCVI') == 1606)
# print roman2arabic('IV')
