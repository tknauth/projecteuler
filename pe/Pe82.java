package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;
// import org.apache.commons.lang.ArrayUtils;

public class Pe82 {
    private static final int default_size = 80;
    private static int size;

    private static class Solution implements Comparable<Solution> {
        int cost;
        int x;
        int y;

        Solution() {
            cost = x = y = 0;
        }
        Solution(int x, int y, int cost) {
            this. x = x;
            this.y = y;
            this.cost = cost;
        }
        public int compareTo(Solution o) {
            return this.cost - o.cost;
        }
        public String toString() {
            return ""+x+" "+y+" "+cost;
        }
    }

    private static int[][] readInput(String fn) {
        int[][] a = new int[size][size];
        try {
            BufferedReader br = new BufferedReader(new FileReader(fn));
            int y = 0;
            do {
                int x = 0;
                String s = br.readLine();
                if (null == s) return a;
                // Arrays.fill(a[row], 0);
                String[] split = s.split(",");
                // System.err.println(s);
                for (String nr : split) {
                    a[x][y] = Integer.parseInt(nr);
                    x++;
                }
                y++;
            } while (true);
        } catch (FileNotFoundException e) {
            System.exit(1);
        } catch (IOException e) {
            System.exit(1);
        }
        throw new RuntimeException();
    }

    public static void insert(List<Solution> l, Solution e) {
        int idx = Collections.binarySearch(l, e);
        if (idx < 0) idx = -idx-1;
        l.add(idx, e);
    }

    public static void main(String[] args) {
        test();
        if (args.length > 1) {
            size = Integer.parseInt(args[1]);
        }

        int[][] a = readInput(args[0]);

        // System.out.println(""+a[0][0]);
        // System.out.println(""+a[size-1][0]);
        // System.out.println(""+a[0][size-1]);
        // System.out.println(""+a[size-1][size-1]);

        int[][] c = new int[size][size];
        for (int i=0; i<size; ++i) {
            for (int j=0; j<size; ++j) {
                c[i][j] = Integer.MAX_VALUE;
            }
        }
        List<Solution> l = new LinkedList<Solution>();

        for (int i=0; i<size; ++i) {
            Solution sol = new Solution(0, i, a[0][i]);
            insert(l, sol);
            c[0][i] = a[0][i];
        }

        do {
            Solution s = l.remove(0);
            // System.out.println("- "+s.x+" "+s.y+" "+s.cost);

            if (s.y > 0) {
                if (c[s.x][s.y]+a[s.x][s.y-1] < c[s.x][s.y-1]) {
                    c[s.x][s.y-1] = c[s.x][s.y]+a[s.x][s.y-1];
                    Solution t = new Solution(s.x, s.y-1, c[s.x][s.y-1]);
                    // System.out.println("+ "+t.x+" "+t.y+" "+t.cost);
                    insert(l, t);
                }
            }
            if (s.y < size-1) {
                if (c[s.x][s.y]+a[s.x][s.y+1] < c[s.x][s.y+1]) {
                    c[s.x][s.y+1] = c[s.x][s.y]+a[s.x][s.y+1];
                    Solution t = new Solution(s.x, s.y+1, c[s.x][s.y+1]);
                    // System.out.println("+ "+t.x+" "+t.y+" "+t.cost);
                    insert(l, t);
                }
            }
            if (c[s.x][s.y]+a[s.x+1][s.y] < c[s.x+1][s.y]) {
                c[s.x+1][s.y] = c[s.x][s.y]+a[s.x+1][s.y];
                Solution t = new Solution(s.x+1, s.y, c[s.x+1][s.y]);
                // System.out.println("+ "+t.x+" "+t.y+" "+t.cost);
                insert(l, t);
            }
            
        } while (l.get(0).x < size-1);

        System.out.println(""+l.get(0).cost);
        // System.out.println(""+Arrays.deepToString(l.toArray()));
        // System.out.println(""+a[size-1][size-1]);
    }

    private static void test() {
    }
}
