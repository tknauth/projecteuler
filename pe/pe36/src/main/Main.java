package main;

public class Main {
	static private int sum = 0;

	public static void main(String[] args) {
		f(new StringBuffer(""));
		for (int n=0; n<10; n++) {
			f(new StringBuffer(Integer.toString(n)));
		}
		System.out.println(""+sum);
	}
	private static void f(StringBuffer sb) {
		System.out.println(sb.toString());
		
		if (sb.length()>0 && sb.charAt(sb.length()-1)!='0') {
			String base2 = Integer.toString(Integer.parseInt(sb.toString()), 2);
			if (isPalindrome(base2)) {
				System.out.println(sb.toString()+"_10, "+base2+"_2");
				sum += Integer.parseInt(sb.toString());
			}
		}
		if (sb.length() < 5) {
			for (int n=0; n<10; n++) {
				String n_str = Integer.toString(n);
				f(new StringBuffer(n_str+sb.toString()+n_str));
			}
		}
	}
	static private boolean isPalindrome(String s) {
		for (int i=0; i<s.length()/2; i++) {
			if (s.charAt(i) != s.charAt(s.length()-i-1)) return false;
		}
		return true;
	}
}
