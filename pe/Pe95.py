import math
import array
from math import floor, sqrt

# def sieve() :
#     primes = array.array('L')
#     a = array.array('L', [0]*(10**6))
#     limit = int(floor(math.sqrt(10**6)+0.5))

#     for factor in xrange(2, limit) :
#         for i in xrange(2*factor, limit, factor) :
#             a[i] += 1

#     for i in xrange(2, limit, 1) :
#         if a[i] == 0 :
#             primes.append(i)
#             # print i

#     return primes

# primes = sieve()

# def prime_factors(x) :
#     factors = []
#     for p in primes :
#         while (x/p)*p == x :
#             factors.append(p)
#             x /= p
#         if x == 1 : return factors

#     return factors

# print prime_factors(220)
# print prime_factors(228)

# for i in xrange(2, 10**6) :
#     f = prime_factors(i)
#     if (i%10000)==0 : print i, f#, primes

# sieve()

a = array.array('L', [1]*(10**6))
limit = 10**6

# First I thought I needed prime factorization to compute the propoer
# divisor sum. By looking at the Erathotenes sieve for prime
# generation, I realized that the divisor sum can be generatred this
# way too: a[n] is the divisor sum for number n.

for factor in xrange(2, limit) :
    for i in xrange(2*factor, limit, factor) :
        a[i] += factor

# print a[11]
# print a[25]
# print a[50]
# print a[640636]
# print a[629072]

# def chain_length(chain, element) :
#     assert element in chain
#     idx = chain.index(element)
#     return len(chain)-idx

# print chain_length([1,2,3,4], 2)
# print chain_length([1,2,3,4], 1)
# print chain_length([5916, 9204L, 14316L, 17716L, 19116L, 19916L, 22744L, 22976L, 31704L, 45946L, 47616L, 48976L, 83328L, 97946L, 122410L, 152990L, 177792L, 243760L, 274924L, 275444L, 285778L, 294896L, 295488L, 358336L, 366556L, 376736L, 381028L, 418904L, 589786L, 629072L], 589786)

# Then it is a simple exercise in determining the chain lengths. For
# each number follow chain until either (1) hit a prime number; chain
# ends with 1. (2) Hit a sum that is greater than the limit 10**6. (3)
# Determine a loop, measure length of loop.

maxlen = 0
c = array.array('l', [0]*(10**6))
for i in xrange(2, limit) :
    if c[i] != 0 : continue
    chain = [i]
    
    # while a[chain[-1]] != chain[0] and a[chain[-1]] != 1 :
    while not (a[chain[-1]] in chain) and a[chain[-1]] != 1 :
        # print i, chain, chain[-1]
        if a[chain[-1]] >= 10**6 : break
        chain.append(a[chain[-1]])

    if a[chain[-1]] in chain : # we have a loop
        idx = chain.index(a[chain[-1]])
        for i in range(len(chain)) :
            if i < idx: c[i] = -1
            else : c[i] = len(chain)-idx

        if (len(chain)-idx) > maxlen :
            # print idx, len(chain), a[chain[-1]], chain[-1]
            # print chain
            maxlen = len(chain)-idx
            print chain[idx]
    else :
        for i in range(len(chain)) : c[i] = -1
