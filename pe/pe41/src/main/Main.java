package main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Main {
	public static void main(String[] args) {
		int p_max = 0;
		boolean digits[] = new boolean[10];
		
		try {
			BufferedReader br = new BufferedReader(new FileReader("/home/thomas/playground/python/primes.db"));
			outer: do {
				String s = br.readLine();
				if (null == s) break;
				Arrays.fill(digits, false);
				for (int i=0; i<s.length(); i++) {
					digits[s.charAt(i)-'0'] = true;
				}
				if (digits[0]==true) continue outer;
				for (int i=1; i<=s.length(); i++) if (digits[i]==false) continue outer;
				for (int i=s.length()+1; i<10; i++) if (digits[i]==true) continue outer;
				int p = Integer.parseInt(s);
				if (p > p_max) p_max = p;
			} while(true);
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
		System.out.println(""+p_max);
	}
}
