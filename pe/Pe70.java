package pe;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.List;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedList;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;
import java.util.Iterator;
import pe.Pe58;

public class Pe70 {

    static final int N = 10000000;

    static ArrayList<Integer> primes = new ArrayList<Integer>();

    public static void main(String[] args) {
        int result_n = 2;
        double result_n_over_phi_for_n = Double.MAX_VALUE;

        //Pe58.readPrimes(primes, N);
        System.err.println(System.currentTimeMillis()+" start primes");
        primes.add(2);
        for (int n=3; n<=N; n+=2) {
            if (Pe58.mrPrimalityProven(n)) primes.add(n);
        }
        System.err.println(System.currentTimeMillis()+" end primes");

        test();

        for (int n=2 ; n<=N; ++n) {
            int phi = phi(n, primes);
            if (isPermutation(phi, n)) {
                //System.out.println(""+n+"\t\t"+phi);
                if (n/(double)phi < result_n_over_phi_for_n) {
                    result_n_over_phi_for_n = n/(double)phi;
                    result_n = n;
                }
            }
        }
        System.out.println(""+result_n);
    }

    public static boolean isPermutation(int a, int b) {
        int a_digits[] = new int[10];
        int b_digits[] = new int[10];

        final String a_str = Integer.toString(a);
        final String b_str = Integer.toString(b);
        if (a_str.length() != b_str.length()) return false;

        for (int i=0; i<a_str.length(); ++i) {
            a_digits[a_str.charAt(i)-'0']++;
        }
        for (int i=0; i<b_str.length(); ++i) {
            b_digits[b_str.charAt(i)-'0']++;
        }
        for (int i=0; i<10; ++i) {
            if (a_digits[i] != b_digits[i]) return false;
        }

        return true;
    }

    public static HashMap<Integer, Integer> primeFactors(int n, List<Integer> primes) {
        if (n <= 0) throw new IllegalArgumentException();

        HashMap<Integer, Integer> factor = new HashMap<Integer, Integer>();
        if (n == 1) {
            factor.put(1,1);
            return factor;
        }

        int tmp = n;
        Iterator<Integer> pitr = primes.iterator();
        int p = pitr.next();
        while (Collections.binarySearch(primes, tmp) < 0) {
            if (((int)Math.floor((tmp/(double)p)))*p == tmp) {
                Integer multiplicity = factor.get(p);
                if (null == multiplicity) {
                    factor.put(p, 1);
                } else {
                    factor.put(p, multiplicity+1);
                }
                tmp /= p;
            } else {
                p = pitr.next();
            }
        }
        Integer multiplicity = factor.get(tmp);
        if (null == multiplicity) {
            factor.put(tmp, 1);
        } else {
            factor.put(tmp, multiplicity+1);
        }
        // System.err.println(n+" "+factor);
        return factor;
    }

    public static int phi(int n, List<Integer> primes) {
        if (n <= 0) throw new IllegalArgumentException();
        if (n == 1) return 1;
        HashMap<Integer, Integer> factor = primeFactors(n, primes);
        int phi = 1;
        for (Map.Entry<Integer, Integer> x : factor.entrySet()) {
            phi *= (x.getKey()-1)*(int)Math.pow(x.getKey(), x.getValue()-1);
        }
        return phi;
    }

    private static void test() {
        assert phi(1, primes) == 1;
        assert phi(2, primes) == 1;
        assert phi(3, primes) == 2;
        assert phi(4, primes) == 2;
        assert phi(5, primes) == 4;
        assert phi(6, primes) == 2;
        assert phi(80, primes) == 32;
    }
}
