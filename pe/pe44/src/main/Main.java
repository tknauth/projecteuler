package main;

public class Main {

	public static void main(String[] args) {
		final int LIMIT=4000;
		int stride=1;
		stride: do {
			for (int n=1; n<=LIMIT; n++) {
				long pn = (3*n-1)*n/2;
				long pn_stride = (3*(n+stride)-1)*(n+stride)/2;
				long d = pn_stride - pn;
				if (isPentNr(d)) {
					long s = pn + pn_stride;
					// If overflow, move on to bigger stride
					if (s < pn) continue stride;
					if (isPentNr(s)) {
						System.out.println("sol: p_k= "+pn+", p_j= "+pn_stride+", d= "+d);
					}
				}
			}
			stride++;
		} while (stride<LIMIT);
	}
	private static boolean isPentNr(long n) {
			long x = ((long)Math.sqrt(24*n+1L)+1L)/6L;
			return x*(3L*x-1L)/2L==n;
	}
}
