package main;

import java.util.ArrayList;
import java.util.Collections;


public class Main {

	private static String[] w = new String[1001];
	private static int[] correction = new int[10];
	private static int MAX = 1001;
	/**
	 * @param args
	 */

	public static int charsInString(String s) {
		int count = 0;
		for (Character c : s.toCharArray()) {
			if (c>='a' && c<='z' || c>='A' && c<='Z') count++;
		}
		return count;
	}
	
	public static void main(String[] args) {
		prepopulate();
		
		assert lettersForNumber(342) == charsInString("three hundred and forty-two");
		assert lettersForNumber(115) == charsInString("one hundred and fifteen");
		assert lettersForNumber(1) == charsInString("one");
		assert lettersForNumber(2) == charsInString("two");
		assert lettersForNumber(11) == charsInString("eleven");
		assert lettersForNumber(12) == charsInString("twelve");
		assert lettersForNumber(111) == charsInString("one hundred and eleven");
		assert lettersForNumber(1000) == charsInString("one thousand");
		assert lettersForNumber(777) == charsInString("seven hundred and seventy-seven");
		assert lettersForNumber(717) == charsInString("seven hundred and seventeen");
		assert lettersForNumber(710) == charsInString("seven hundred and ten");		
		assert lettersForNumber(706) == charsInString("seven hundred and six");
		// Will fail! Just subtract 9*3 from the result to get the correct result.
		assert lettersForNumber(700) == charsInString("seven hundred");

		int totalChars = 0;
		for (int i=1; i<MAX; i++) {
			totalChars += lettersForNumber(i);
		}
		System.out.println("totalChars= "+totalChars);
	}
	
	private static int lettersForNumber(int n) {
		int charsForThisNr = 0;
		String s = (new StringBuffer(Integer.toString(n))).reverse().toString();
		char[] i_as_s = s.toCharArray();
		for (int c=0; c<i_as_s.length; c++) {
			assert i_as_s[c]-'0' >= 0 && i_as_s[c]-'0' < 10;
			int w_index = (i_as_s[c]-'0')*(int)Math.pow(10, c);
			charsForThisNr += (w[w_index]).length();
			if (c==1 && i_as_s.length>=2 && i_as_s[1]=='1') {
				charsForThisNr += correction[i_as_s[0]-'0'];
			}
		}
		System.out.println("n= "+n+" len= "+charsForThisNr);
		return charsForThisNr;
	}
	
	public static void prepopulate() {
		w[0] = "";
		w[1] = "one";
		w[2] = "two";
		w[3] = "three";
		w[4] = "four";
		w[5] = "five";
		w[6] = "six";
		w[7] = "seven";
		w[8] = "eight";
		w[9] = "nine";
		w[10] = "ten";
		w[11] = "eleven";
		w[12] = "twelve";
		w[13] = "thirteen";
		w[14] = "fourteen";
		w[15] = "fifteen";
		w[16] = "sixteen";
		w[17] = "seventeen";
		w[18] = "eighteen";
		w[19] = "nineteen";
		w[20] = "twenty";
		w[30] = "thirty";
		w[40] = "forty";
		w[50] = "fifty";
		w[60] = "sixty";
		w[70] = "seventy";
		w[80] = "eighty";
		w[90] = "ninety";
		w[100] = "onehundredand";
		w[200] = "twohundredand";
		w[300] = "threehundredand";
		w[400] = "fourhundredand";
		w[500] = "fivehundredand";
		w[600] = "sixhundredand";
		w[700] = "sevenhundredand";
		w[800] = "eighthundredand";
		w[900] = "ninehundredand";
		w[1000] = "onethousand";
		
		correction[0] = 0;
		// 6 (one ten) - 6 (eleven) = 0
		correction[1] = 0;
		// 6 (two ten) - 6 (twelve) = 0
		correction[2] = 0;
		// 8 (three ten) - 8 (thirteen)
		correction[3] = 0;
		// 7 four ten - 8 fourteen
		correction[4] = 1;
		// 7 five ten - 7 fifteen
		correction[5] = 0;
		// 6 six ten - 7 sixteen
		correction[6] = 1;
		// 8 seven ten - 9 seventeen
		correction[7] = 1;
		// 8 eight ten - 8 eighteen
		correction[8] = 0;
		// 7 nine ten - 8 nineteen
		correction[9] = 1;
	}
}
