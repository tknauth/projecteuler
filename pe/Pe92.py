#!/usr/bin/env python

# Unimaginative brute force solution. Only smarts is in not converting
# int to string to count decimal places.

def chain_number(n) :
    cnr = 0
    while n > 9 :
        last_digit = n-(n/10)*10
        # print 'last_digit', last_digit
        n /= 10
        cnr += last_digit*last_digit
    cnr += n * n
    return cnr

def arrives_at_89(n) :
    while n != 89 and n != 1 :
        # print 'n', n
        if 
        n = chain_number(n)
    if n == 89 : return True
    return False

# print arrives_at_89(44)
# print arrives_at_89(85)

count = 0
for i in range(1, 10000000) :
    if (i % 10000) == 0 : print i
    if arrives_at_89(i) :
        count += 1

print count

