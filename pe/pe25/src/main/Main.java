package main;

import java.math.BigInteger;

public class Main {
	public static void main(String[] args) {
		BigInteger a[] = new BigInteger[2];
		a[0] = BigInteger.valueOf(1);
		a[1] = BigInteger.valueOf(1);
		int i = 3;
		do {
			a[i%2] = a[i%2].add(a[(i+1)%2]);
			if (a[i%2].toString().length()>=1000) {
				System.out.println(a[i%2].toString());
				System.out.println(i);
				break;
			}
			i++;
		} while (true);
	}
}
